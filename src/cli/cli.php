<?php

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

$optionArgCount = 0;
$docTypeOpt = '';
$dataTypeOpt = '';
$documentURL = '';
$xpathQuery = '';

//$argv = [ "C:\Users\garet\Documents\NetBeansProjects\scraperlite\src\cli\cli.hp",
//    "-e", "el", "http://gso.sdf-eu.org" ];
//$argc = count($argv);

//print_r($argv);

$optionAry = getopt('n:', ['node-type:']);  //TODO check hyphenated opt.
if (is_bool($optionAry) and $optionAry = FALSE) {
    print "Usage..." . PHP_EOL;
    exit(3);
}


function usage() {
    print <<<EOT
Usage:
    
php src/cli/cli.php [ -n|--node-type <node type string> ] <url> [ <XPath expr.> ]

-n|--node-type          optional, data type of XPath node(s) queried (specify an XPath expression)
<node type string>      element|attribute|htmllist|htmltable (abbreviated forms can be given - 'el', 'at', etc.)
<url>                   URL of web page to retrieve
<XPath expression>      XPath 1.0 expression
    
A URL arguement alone retrieves the source code of the web page.

Given an URL and a XPath expression as arguements the script returns the XPath query result (XML format).
    
If a node type argument is also given then the script returns the text content of the XPath query result(s).

EOT;
    exit(3);
}

//print_r($optionAry);

//TODO check for valid or duplicate options
//TODO code curl options
//TODO Usage sub.
//TODO exit sub.

// Document type option (default web page).

// ...

// Data element option (if not specified script returns the whole document).

foreach (array_keys($optionAry) as $opt) { 
    switch ($opt) {
        case 'node-type':
        case 'n':
            if (strncmp('el', $optionAry[$opt], 2) === 0) {
                $dataTypeOpt = 'element';
            }
            elseif (strncmp('at', $optionAry[$opt], 2) === 0) {
                $dataTypeOpt = 'attribute';
            }
            elseif (strncmp('htmlli', $optionAry[$opt], 6) === 0) {
                $dataTypeOpt = 'htmllist';
            }
            elseif (strncmp('htmlta', $optionAry[$opt], 6) === 0) {
                $dataTypeOpt = 'htmltable';
            }
            else {
                print "Usage..." . PHP_EOL;
                exit(3);
            }
            $optionArgCount += 2;
            break;
        default:
            usage();
    }
}
//print $dataTypeOpt . PHP_EOL;

// URL and XPath expression arguments.

$operandArgCount = $argc - 1 - $optionArgCount;  // $argv[0] script name
switch ($operandArgCount) {
    case 1:  //TODO validate as URL
        $documentURL = $argv[$optionArgCount + 1];
        $xpathQuery = '';  // default return whole document
        break;
    case 2:
        $documentURL = $argv[$optionArgCount + 1];
        $xpathQuery = $argv[$optionArgCount + 2];
        break;
    default:
            usage();
}
//print $documentURL . PHP_EOL;
//print $xpathQuery . PHP_EOL;

//TODO The problem with PHP's outdated CA certs solved for now by disabling
// certificate checking, the correct solution appears to be outlined here:
// http://stackoverflow.com/questions/316099/cant-connect-to-https-site-using-curl-returns-0-length-content-instead-what-c

$curl_opt = [ CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_SSL_VERIFYHOST => 2 
    ];

if ($dataTypeOpt === '' and $xpathQuery === '') {  // ouptut source
    print (new \ScraperLite\DOMDocumentData(
        new \ScraperLite\WebPage($documentURL, $curl_opt),
        '/'  //TODO check valid xpath!
    ))->queryResult();
}
elseif ($dataTypeOpt === '' and $xpathQuery !== '') {  // ouptut source
    print (new \ScraperLite\DOMDocumentData(
        new \ScraperLite\WebPage($documentURL, $curl_opt),
        $xpathQuery
    ))->queryResult();
}
elseif ($dataTypeOpt === 'element') {  // query for element text value
    print (new \ScraperLite\ElementText(
        new \ScraperLite\WebPage($documentURL, $curl_opt),
        $xpathQuery
    ))->queryResult();
}
elseif ($dataTypeOpt === 'attribute') {  // query for element text value
    print (new \ScraperLite\AttributeValue(
        new \ScraperLite\WebPage($documentURL, $curl_opt),
        $xpathQuery
    ))->queryResult();
}
elseif ($dataTypeOpt === 'htmllist') {
    $ary = (new \ScraperLite\HTMLList(
        new \ScraperLite\WebPage($documentURL, $curl_opt),
        $xpathQuery
    ))->queryResult();
    foreach (array_values($ary) as $el) {
        print $el . PHP_EOL;
    }
//        var_dump($ary);
}
elseif ($dataTypeOpt === 'htmltable') {
    $ary = (new \ScraperLite\HTMLTable(
        new \ScraperLite\WebPage($documentURL, $curl_opt),
        $xpathQuery
    ))->queryResult();
    foreach (array_values($ary) as $row) {
        foreach (array_values($row) as $col) {
            print $col . PHP_EOL;
        }
        print PHP_EOL;
    }
//        var_dump($ary);
}
else {  // usage error
            usage();
}
