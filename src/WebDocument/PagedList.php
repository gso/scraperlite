<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Iterate through list items that are paged (e.g., typically search results).
 * 
 * @todo merge PagingWebsite in as a trait
 */
class PagedList extends AbstractWebDocument implements \IteratorAggregate
{

    public $pageUrlCallback;  // callback to return URL of page
    public $listIteratorCallback;

    /**
     * 
     * @param callable $pageUrlCallback See {@link PagingWebsite::__construct}.
     * @param callable $listIteratorCallback Callback returning an iterator object for the 
     * the list items on the current page given as its first arg. the 
     * `PagedList` object with the curent iterator page loaded.
     * @param array $curlOptions See {@link curl_fetch()}.
     */
    public function __construct(
        callable $pageUrlCallback,
        callable $listIteratorCallback,
        array $curlOptions = null
    ) {
        parent::__construct(null, $curlOptions);
        $this->pageUrlCallback = $pageUrlCallback;
        $this->listIteratorCallback = $listIteratorCallback;
    }

    protected function loadDomDocument() {  // a WebPage trait
        return $this->domDocument()->loadHTML($this->source());
    }
    
    public function pageUrlAtOffset($offset) {
        assert(filter_var_validate_offset($offset));
        return call_user_func($this->pageUrlCallback, $this, $offset);        
    }
    
    public function loadPageAtOffset($offset) {
        assert(filter_var_validate_offset($offset));
        $this->setUrl($this->pageUrlAtOffset($offset));
    }

    /**
     * Iterator iterating through the list items consecutively on each page 
     * in turn.
     * 
     * @return PagedListIterator
     */
    public function getIterator(callable $callback = null) {
        return new PagedListIterator(
            $this,
            $this->listIteratorCallback,
            $callback
        );
    }

}
