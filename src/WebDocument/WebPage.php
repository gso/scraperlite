<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve an HTML web page.
 * 
 */
class WebPage extends AbstractWebDocument
{

//    use WebPageTrait;

    /**
     * 
     * @param string $url See corresponding {@see curl_fetch} argument.
     * @param array $curlOptions See corresponding {@see curl_fetch} argument.
     */
    public function __construct($url, array $curlOptions = []) {
        parent::__construct($url, $curlOptions);
    }

    protected function loadDomDocument() {
        return $this->domDocument()->loadHTML($this->source());
    }

}
