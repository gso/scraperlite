<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Iterate through the pages of a paged website.
 * 
 */
class PagingWebsite extends AbstractWebDocument implements \IteratorAggregate
{

    private $pageUrlCallback;

    /**
     * 
     * @param callable $pageUrlCallback Callback returning an URL of a page 
     * of a paging webiste 
     * given the zero offset of the page to be retrieved - if a page for the 
     * given offset does not exist then an empty string should be returned.
     * See code examples for usage.
     * @param array $curlOptions See {@link curl_fetch()}.
     */
    public function __construct(
        callable $pageUrlCallback,  // return a URL given the current page iterator offset count
        array $curlOptions = null
    ) {
        parent::__construct(null, $curlOptions);
        $this->setPageUrlCallback($pageUrlCallback);
    }

    public function setPageUrlCallback($pageUrlCallback) {
        $this->pageUrlCallback = $pageUrlCallback;
    }
    
    public function pageUrlCallback() {
        return $this->pageUrlCallback;
    }
    
    protected function loadDomDocument() {  // a trait of WebPage class
        return $this->domDocument()->loadHTML($this->source());
    }
    
    public function pageUrlAtOffset($offset) {
        assert(filter_var_validate_offset($offset));
        return call_user_func($this->pageUrlCallback(), $this, $offset);        
    }
    
    /**
     * Load a page from the website given the zero offset of the 
     * page.
     * 
     * @param integer $offset Zero offset of the page to load.
     */
    public function loadPageAtOffset($offset) {
        assert(filter_var_validate_offset($offset));
        $this->load($this->pageUrlAtOffset($offset));
    }

    /**
     * Iterates through the pages of a paged website.
     * 
     * @return PagingWebsiteIterator
     */
    public function getIterator(callable $callback = null) {
        return new PagingWebsiteIterator($this, $callback);
    }

}
