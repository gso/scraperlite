<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @todo Item callback.
 * @todo accessors, domNode() method
 */
class PagedListIterator implements \Iterator
{
    private $pagedList;
    private $listIteratorCallback;  // returns a page list data iterator
    private $callback;
    private $listIterator;  // iterator returned by list iterator callback
    public $pageOffset;
    public $pageUrl;
    public $listOffset;
    private $noMorePagesFlag;
    private $noMoreListResultsFlag;
    private $noMoreResultsFlag;  // all pages processed

    public function __construct(
        PagedList $pagedList, 
        callable $listIteratorCallback,
        callable $callback = null
    ) {
        $this->pagedList = $pagedList;
        $this->listIteratorCallback = $listIteratorCallback;
        $this->callback = $callback;
    }

    public function rewind() {  // start of loop
        $this->noMoreResultsFlag = false;
        $this->reset(0);
        if ($this->noMorePagesFlag or $this->noMoreListResultsFlag) {
            // no list items on initial page
            // debating whether there being no initial page should be an 
            // exception
            $this->noMoreResultsFlag = true;
        }
    }

    public function current() {
        if (is_null($this->callback)) {
            return $this->listIterator->current();
        } else {
            call_user_func(
                $this->callback, 
                $this, 
                $this->key(), 
                $this->listIterator->current()
            );
        }
    }

    public function key() {
        return $this->listIterator->key();
    }

    public function next() {  // at end of loop
        // next in list of page items
        $this->listIterator->next();
        if ($this->listIterator->valid()) {
            $this->listOffset = $this->listIterator->key();
        } else {
            // either process next page or no more pages to process
            $this->reset($this->pageOffset + 1);
            if ($this->noMorePagesFlag) {
                $this->noMoreResultsFlag = true;
            } elseif ($this->noMoreListResultsFlag) {
                // a new page with no list results on it
                throw new UnexpectedValueException(<<<'EOT'
Pages subsequent to the initial page must have at least one listed item.
EOT
                );
            }
            // new page loaded successfully
        }
    }
    
    private function reset($pageOffset) {
        $this->noMorePagesFlag = false;
        $page_url = $this->pagedList->pageUrlAtOffset($pageOffset);
        if (!empty($page_url)) {  
            // still some more pages to process
            $this->noMoreListResultsFlag = false;
            $this->pageOffset = $pageOffset;
            $this->pageUrl = $page_url;
            $this->pagedList->setUrl($this->pageUrl);
            $this->listIterator = call_user_func(
                $this->listIteratorCallback, 
                $this->pagedList,
                $pageOffset
            );
            assert($this->listIterator instanceof \Iterator, "List iterator callback type");
            $this->listIterator->rewind();
            if (!$this->listIterator->valid()) {
                // error, it looks like a page without any data 
                $this->noMoreListResultsFlag = true;
            }
        } else {  // no more pages to process
            $this->noMorePagesFlag = true;
        }        
    }

    public function valid() {  // after rewind and next
        return !$this->noMoreResultsFlag;
    }

}
