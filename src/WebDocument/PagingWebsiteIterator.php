<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * 
 */
class PagingWebsiteIterator implements \Iterator
{

    public $pagingWebsite;
    private $callback;
    public $pageOffset;
    public $pageUrl;  //TODO factor out?
    private $noMorePagesFlag;

    function __construct(
        PagingWebsite $pagingWebsite, 
        callable $callback = null
    ) {
        $this->pagingWebsite = $pagingWebsite;
        $this->callback = $callback;
    }

    public function rewind() {
        $this->noMorePagesFlag = false;
        $this->reset(0);
    }

    public function current() {
        if (is_null($this->callback)) {
            return $this->pageUrl;        
        } else {
            return call_user_func(
                $callback, 
                $this, 
                $this->pageOffset, 
                $this->pageUrl
            );
        }
    }

    public function key() {
        return $this->pageOffset;
    }

    public function next() {
        $this->reset($this->pageOffset + 1);
    }
    
    private function reset($pageOffset) {
        $page_url = $this->pagingWebsite->pageUrlAtOffset($pageOffset);
        if (!empty($page_url)) {  // empty on not more pages
            $this->pageOffset = $pageOffset;
            $this->pageUrl = $page_url;
            $this->pagingWebsite->setUrl($this->pageUrl);
        } else {
            $this->noMorePagesFlag = true;
        }        
    }

    public function valid() {
        return !$this->noMorePagesFlag;
    }

}
