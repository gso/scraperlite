<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

const WEBDOCUMENT_LOAD_ERR = 1;

/**
 * 
 * Instances of subclasses of `AbstractWebDocument` retrieve documents 
 * from the web which can then
 * can be passed to instances of subclasses of {@link AbstractNodeData} to
 * be queried for data.
 * (Ref. the class tree and `AbstractWebDocument`subclasses
 * for a list of the types of web document that can be retrieved.)
 * 
 * @todo load($url) method
 * 
 * @internal
 */
abstract class AbstractWebDocument
{

    /**
     * @return void ($domDocument set as false on error)
     */
    abstract protected function loadDomDocument();

    /**
     * @var string
     */
    protected $url;    

    private $memoizedUrl;  
    
    /**
     * Ref. {@link curl_fetch()}, see also the 
     * {@link http://curl.haxx.se/libcurl/ `libcurl`} website. 
     * 
     * @var array
     */
    private $curlOptions;
    
    /**
     * @var string
     * @todo rename to 'content'?
     */
    private $source;
    
    /**
     * @var \DOMDocument
     */
    private $domDocument;
    
    /**
     * @var array
     */
    private $libxmlErrors;
   
    /**
     * @var \DOMXPath 
     */
    private $domXPath;

    /**
     *
     * @param string $url See corresponding class property.
     * @param array $curlOptions See corresponding class property.
     */
    public function __construct($url = null, array $curlOptions = []) {
        $this->setUrl($url);
        $this->memoizedUrl = null;
        $this->setCurlOptions($curlOptions);
        $this->domDocument = new \DOMDocument();
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function url() {
        return $this->url;
    }

    public function setCurlOptions(array $curlOptions) {
        $this->curlOptions = $curlOptions;
    }
    
    public function curlOptions() {
        return $this->curlOptions;
    }

    public function load($url = null) {
        if (isset($url)) {
            $this->setUrl($url);
        }
        if ($this->memoizedUrl !== $this->url()) {
            $this->memoizedUrl = $this->url();
            $this->refresh();
        }
    }

    /**
     * 
     * @return void Refresh the current document source.
     */
    public function refresh() {
        $this->source = curl_fetch($this->url(), $this->curlOptions());
        libxml_clear_errors();
        libxml_use_internal_errors(true);
        // parse the document source
        if ($this->loadDomDocument() === false) {
            throw new WebDocumentException(
                'The document to be load must be of a type recognised by '
                . 'the subclass in function ' . __FUNCTION__ . '.',
                WEBDOCUMENT_LOAD_ERR
            );
        }
        $this->libxmlErrors = [];
        foreach (libxml_get_errors() as $error) {
            $this->libxmlErrors[] = $error->message;
        }
        libxml_use_internal_errors(false);  // default
        $this->domXPath = new \DOMXPath($this->domDocument());
    }

    /**
     * 
     * @return string Source code as originally retrieved from the Web.
     */
    public function source() {
        $this->load();
        return $this->source;
    }


    /**
     * 
     * @return \DOMDocument Parsed document source,
     * set to false on error 
     * parsing, otherwise a PHP `DOMDocument` object
     */
    public function domDocument() {
        $this->load();
        return $this->domDocument;
    }
    
    /**
     * 
     * @return array Errors and warnings returned by `libxml` while 
     * parsing the document source.
     */
    public function libxmlErrors() {
        $this->load();
        return $this->libxmlErrors;
    }

    /**
     * 
     * @return \DOMXPath Document PHP `DOMXPath` object.
     */
    public function domXPath() {
        $this->load();
        return $this->domXPath;
    }

}
