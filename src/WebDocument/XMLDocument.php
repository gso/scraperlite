<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve a XML document.
 */
class XMLDocument extends AbstractWebDocument
{

    public function __construct($url, array $curlOptions = null) {
        parent::__construct($url, $curlOptions);
    }

    protected function loadDomDocument() {
        return $this->domDocument()->loadXML($this->source());
    }

}
