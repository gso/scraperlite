<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

//TODO refactor

// E.g., multiple DOM nodes returned when the XPath should only match one
const DOMXPATH_QUERY_NODE_ERR = 1;
// see curl_fetch()
const DOMXPATH_QUERY_FAIL_ERR = 2;

/**
 * Evaluate a XPath expression against a DMXPath object returning a single
 * DOMNode object .
 * 
 * @todo Test units
 */
function query_domxpath_for_domnode(
    \DOMXPath $domXPath, 
    $xPathQuery, 
    $queryContextDomNode = null
) {
    $domnodelist = query_domxpath_for_domnodelist(
        $domXPath,
        $xPathQuery, 
        $queryContextDomNode
    );
    if ($domnodelist->length === 1) {
        return $domnodelist->item(0);
    } elseif ($domnodelist->length === 0 or $domnodelist->length > 1) {
        throw new DOMXPathQueryException(
            'Argument 2 in function '. __FUNCTION__
            . ' must evaluate to a single DOM node, '
            . \strval($domnodelist->length) . ' returned.',
            DOMXPATH_QUERY_NODE_ERR
        );
    } else {
        throw new \RuntimeException('Internal PHP error');
    }
}

/**
 * Evaluate an XPath expression against a DMXPath object returning a
 * DOMNodeList object.
 * 
 * @todo Test units
 */
function query_domxpath_for_domnodelist(
    \DOMXPath $domXPath, 
    $xPathQuery, 
    $queryContextDomNode = null
) {
    \assert(is_string($xPathQuery), 'Argument 2 type');
    // NB a query() fail results in a warning from PHPUnit (seems to
    // overide any axception thrown).
    $domnodelist = $domXPath->query(
        $xPathQuery, 
        $queryContextDomNode
    );
    if ($domnodelist instanceof \DOMNodeList) {
        return $domnodelist;  // possibly an empty DOMNodeList
    } elseif ($domnodelist === false) {
        throw new DOMXPathQueryException(
            'DOMXPath query failed '
            . '(malformed query expression or invalid context node)'
            . ' in function ' . __FUNCTION__ . '.',
            DOMXPATH_QUERY_FAIL_ERR
        );
    } else {
        throw new \RuntimeException('Internal PHP error');
    }
}
