<?php 

namespace ScraperLite;

// See also https://github.com/jwage/purl Purl is a simple Object Oriented 
// URL manipulation library for PHP 5.3+.
// NB These functions are not multidimensional query parameter friendly 
// (accept perhaps url_query_string_add_parameter).

//require 'http-build-url.php';  // shim HTTP ext. (not bundled)

/**
 * Returns the difference between two URL query strings (needs to work doing).
 * 
 * @todo Test units
 */
function url_query_string_diff() {  // args: url1, url2..urln
    // Return array of url1 keys that are not present in or with differing 
    // values in url2..urln
    $query_string_keys_different_ary = array();
    $urlStr = \func_get_arg(0);
    $url_querystr_ary = url_query_string_as_array($urlStr);
    $arg_count = \func_num_args();
    for ($i = 1; $i < $arg_count; $i++) {
        $url2_querystr_ary = url_query_string_as_array(func_get_arg($i));
        foreach ($url_querystr_ary as $key => $value) {
            if (! (\array_key_exists($key, $url2_querystr_ary) and 
                    $url2_querystr_ary[$key] === $value)) {
                $query_string_keys_different_ary[] = $key;
            }
        }
    }
    return \array_unique($query_string_keys_different_ary);
}

/**
 * Return the query string of an URL as an array.
 * 
 * @todo Test units
 */
function url_query_string_as_array($urlStr) {
    $querystr_ary = array();
    $url_components_ary = \parse_url($urlStr);
    if ( \array_key_exists('query', $url_components_ary) ) {
        $querystr = $url_components_ary['query'];
        \parse_str($querystr, $querystr_ary);
    }
    return $querystr_ary;
}

/**
 * Set the value of a query string parameter.
 */
function url_query_string_set_value($urlStr, $key, $value) {
    $querystr_ary = array();
    $url_components_ary = \parse_url($urlStr);
    if ( \array_key_exists('query', $url_components_ary) ) {
        $querystr = $url_components_ary['query'];
        \parse_str($querystr, $querystr_ary);
    }
    $querystr_ary[$key] = $value;
    $url_components_ary['query'] = \http_build_query($querystr_ary);
    return \http_build_url($url_components_ary);
}

/**
 * Get the value of a parameter of an URL query string.
 */
function url_query_string_get_value($urlStr, $key) {
    $query_str_ary = url_query_string_as_array($urlStr);
    return $query_str_ary[$key];
}

/**
 * Remove a parameter from an URL query string.
 */
function url_query_string_delete_key($urlStr, $key) {  // untested
    // the result of duplicate keys is undefined
    $url_components_ary = \parse_url($urlStr);
    $querystr = $url_components_ary['query'];
    $querystr_ary = array();
    \parse_str($querystr, $querystr_ary);
    unset($querystr_ary[$key]);
    $url_components_ary['query'] = \http_build_query($querystr_ary);
    return http_build_url($url_components_ary);    
}

/**
 * Add a parameter to an URL query string.
 */
function url_query_string_add_parameter($urlStr, $key, $value) {  // allows duplicate keys
    $url_components_ary = parse_url($urlStr);
    $querystr = $url_components_ary['query'];
    if ($querystr !== "" and $querystr !== NULL) {
        $querystr .= '&';        
    }
    $querystr .= urlencode($key) . '=' . urlencode($value);
    $url_components_ary['query'] = $querystr;
    return http_build_url($url_components_ary);    
}
