<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Function to 'save' (to a string, as seems to be standard for the DOMNode
 * 'save' methods) the outer HTML of a PHP DOMNode object.
 * 
 * @todo Test units
 */
function domnode_saveouterhtml(\DOMNode $domNode) {
    \assert($domNode->nodeType === XML_ELEMENT_NODE, 'Argument 1 type');
    return \rtrim(domdocument_from_domnode($domNode)->saveHTML());
}
