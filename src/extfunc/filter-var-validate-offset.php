<?php

namespace ScraperLite;

/**
 * Check if a number is valid in an offset type role.
 * 
 * @param integer $offset Offset to be validated.
 * @param integer $upper_bound Optional upper bound, if set then false will be returned
 * if the offset is greater than this value.
 */
function filter_var_validate_offset($offset, $upper_bound = null) {
    \assert(is_int($offset), 'Argument 1 type');
    \assert(is_int($upper_bound) or \is_null($upper_bound), 'Argument 2 type');
    if (\is_null($upper_bound)) {
        return $offset >= 0;
    } else {
        return $offset >= 0 and $offset <= $upper_bound;
    }
}
