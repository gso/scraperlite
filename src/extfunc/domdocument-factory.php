<?php

namespace ScraperLite;

/**
 * Parses HTML returning a PHP DOMDocument object.
 * 
 * @todo Test units
 */
function domdocument_from_html(&$html) {
    \assert(is_string($html), 'Argument 1 type');
    $domdocument = new \DOMDocument();
    \libxml_use_internal_errors(true);
    // warnings issued if $html is an empty string or malformed HTML
    if (!$domdocument->loadHTML($html)) {  // TODO function
        $libxml_err = '';
        foreach (\libxml_get_errors() as $error) {
            $libxml_err .= $error->message . PHP_EOL;
        }
        \libxml_clear_errors();        
        throw new \InvalidArgumentException(
            'DOM load error' . PHP_EOL 
            . $libxml_err
        );
    }
    return $domdocument;
}

/**
 * Parses XML returning a PHP DOMDocument object.
 * 
 * @todo Test units
 */
function domdocument_from_xml(&$xml) {
    assert(is_string($xml), 'Argument 1 type');
    $domdocument = new \DOMDocument();
    libxml_use_internal_errors(true);
    if (!$domdocument->loadXML($xml)) {
        $libxml_err = '';
        foreach (libxml_get_errors() as $error) {
            $libxml_err .= $error->message . PHP_EOL;
        }
        libxml_clear_errors();        
        throw new \InvalidArgumentException(
            'DOM load error' . PHP_EOL 
            . $libxml_err
        );
    }
    return $domdocument;
}

/**
 * Converts a non-DOMDocument DOMNode to a DOMDocument.
 * 
 * @todo Test units
 */
function domdocument_from_domnode(\DOMNode $domNode) {
    assert(
        !($domNode instanceof \DOMDocument), 
        'Argument 1 type must not be an instance of DOMDocument'
        . ' (cannot append a child of type DOMDocument to a DOMDocument'
    );
    $domdocument = new \DOMDocument();
    $domdocument->appendChild(
        $domdocument->importNode($domNode, true)  // copy of node
    );
    return $domdocument;
}

/**
 * Converts a DOMNodeList of DOMNode objects to a DOMDocument.  The function
 * will fail if any of the DOMNodeList items themsevles are DOMDocument nodes.
 * 
 * @todo Test units
 */
function domdocument_from_domnodelist(\DOMNodeList $domNodeList) {
    $domdocument = new \DOMDocument();
    foreach ($domNodeList as $domnode) {
        assert(
            !($domnode instanceof \DOMDocument), 
            'Argument 1 type must not be an instance of DOMDocument'
            . ' (cannot append a child of type DOMDocument to a DOMDocument'
        );
        $domdocument->appendChild(
            $domdocument->importNode($domnode, true)
        );
    }
    return $domdocument;
}
