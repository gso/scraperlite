<?php

namespace ScraperLite;

// E.g., multiple DOM nodes returned when the XPath should only match one
//const DOMXPATHQUERY_RESULT_ERR = 1;  
//const DOMXPATHQUERY_FAIL_ERR = 3;

class DOMXPathQueryException extends \Exception
{
    
    public function __construct(
        $message, 
        $code = 0, 
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

}
