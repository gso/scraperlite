<?php

namespace ScraperLite;

/**
 * Quick function to validate a RFC 2396 valid URL string (see source code
 * re. issues).
 * 
 * @todo Test units
 */
function filter_var_validate_url($url) {
    \assert(\is_string($url), 'Argument 1 type');
    // ref. comment #2 http://nl1.php.net/manual/en/function.preg-replace.php)
    \mb_internal_encoding('UTF-8');
    if(\filter_var(  //TODO quick hack, international chars not supported in URLs
        \preg_replace('/[[:^ascii:]]/u', 'x', $url),  // convert to ASCII enc.?
        FILTER_VALIDATE_URL 
    )) {
        return true;
    } else {
        return false;
    }
}

// Notes:
// PHP 6 UTF-8 default  http://php.net/manual/en/refs.international.php
// PHP 5.6 default mb_* regex encoding changed to UTF-8 http://php.net/manual/en/function.mb-regex-encoding.php
// Strategy for Handing Character Encoding in PHP Applications http://www.phpwact.org/php/i18n/charsets
// PHP and UTF-8 Howto - Experiences from WebCollab http://webcollab.sourceforge.net/unicode.html
