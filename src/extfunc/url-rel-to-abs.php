<?php 

namespace ScraperLite;

// http://99webtools.com/relative-path-into-absolute-url.php retrieved 19 Jun 2014
// Originally named 'rel2abs'.
// See also: http://fil.ya1.ru/PHP_5_in_Practice/scr/0768667437/ch08lev1sec5.html
// and http://www.moyablog.com/2011/10/19/how-to-convert-relative-urls-to-absolute-urls-in-websites-using-php/


/**
 * Return an absolute URL given a relative URL.
 * 
 * @todo Test units
 */
function url_rel_to_abs($rel, $base) {
    if (\strpos($rel, "//") === 0) {
        return "http:" . $rel;
    }
    /* return if  already absolute URL */
    if (\parse_url($rel, PHP_URL_SCHEME) != '')
        return $rel;
    /* queries and  anchors */
    if ($rel[0] == '#' || $rel[0] == '?')
        return $base . $rel;
    /* parse base URL  and convert to local variables:
      $scheme, $host,  $path */
    extract(parse_url($base));
    /* remove  non-directory element from path */
    $path = \preg_replace('#/[^/]*$#', '', $path);
    /* destroy path if  relative url points to root */
    if ($rel[0] == '/')
        $path = '';
    /* dirty absolute  URL */
    $abs = "$host$path/$rel";
    /* replace '//' or  '/./' or '/foo/../' with '/' */
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for ($n = 1; $n > 0; $abs = \preg_replace($re, '/', $abs, -1, $n)) { }
    /* absolute URL is  ready! */
    return $scheme . '://' . $abs;
}
