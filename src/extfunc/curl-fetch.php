<?php

namespace ScraperLite;

const CURLFETCH_CURL_INIT_ERR = 1;
const CURLFETCH_CURLOPT_ERR = 2;
const CURLFETCH_CURL_EXEC_ERR = 3;

/**
 * Fetch a document from the Web using the PHP cURL extension.
 * @todo Test units
 * @todo Retrieve headers.
 * 
 * @param string URL of the web page to be retrieved.
 * @param array {@link http://php.net/manual/en/function.curl-setopt.php cURL session 
 * options} (see also {@link http://curl.haxx.se/ libcurl}).
 * @return string The source code of the web document.
 * @throws CurlFetchException code `CURLFETCH_CURL_INIT_ERR` if cURL fails to initialise (i.e., return a cURL handle).
 * @throws CurlFetchException code `CURLFETCH_CURLOPT_ERR` on a non-valid cURL option
 * @throws CurlFetchException code `CURLFETCH_CURL_EXEC_ERR` on error retrieving the Web page
 */

function &curl_fetch($url, array $curlOptions = null) {
    \assert(filter_var_validate_url($url), 'Argument 1 type');  // RFC 2396 URL
    $ch = \curl_init($url);  // curl handle
    if (!$ch) {
        throw new CurlFetchException(
            'Failed to initialise cURL', 
            CURLFETCH_CURL_INIT_ERR
        );
    }
    elseif (\curl_errno($ch)) {
        \curl_close($ch);
        throw new CurlFetchException(
            'Curl error: ' . curl_error($ch),
            CURLFETCH_CURL_INIT_ERR
        );
    }
    if (isset($curlOptions)) {
        foreach ($curlOptions as $option => $value) {
            if (!\curl_setopt($ch, $option, $value)) {
                \curl_close($ch);
                throw new CurlFetchException(
                    'Error setting option: ' . $option . ' => ' . $value, 
                    CURLFETCH_CURLOPT_ERR
                );
            }
        }
    }
    \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  // return page as a string
    $document_source = \curl_exec($ch);
    // Returned a "not a valid cURL handle resource" err. on apache not being started
    // (the handle was fine)!
    if (\curl_errno($ch)) {
        \curl_close($ch);
        throw new CurlFetchException(
            'Curl error: ' . curl_error($ch),
            CURLFETCH_CURL_EXEC_ERR
        );
    }
    \curl_close($ch);
    return $document_source;
}
