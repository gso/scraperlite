<?php

namespace ScraperLite;
//
//const CURLFETCH_CURL_INIT_ERR = 1;
//const CURLFETCH_CURLOPT_ERR = 2;
//const CURLFETCH_CURL_EXEC_ERR = 3;

class CurlFetchException extends \Exception
{
    
    public function __construct(
        $message, 
        $code = 0, 
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

}
