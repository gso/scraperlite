<?php

namespace ScraperLite;

/**
 * Process a `filter_var` like `$options` argument  ** unfinished, do not 
 * use **.
 */
function filter_options($options, $definition, $flags = 0) {
    if (\is_array($options)) {  // param. is an options array
        // can have either or both option groups, but no other keys are allowed
        if (\array_diff_key($options, ['options', 'flags']) === []) 
        {
            // move to rubroutines
            if (
                \array_key_exists('flags', $options) 
                and !\is_array($options['options'])
            ) {
                // return false
            }
            if (
                \array_key_exists('flags', $options) 
                and !( 
                    \is_array($options['flags'])  // array of flags
                    or \is_integer($options['flags'])  // bitfield
                )
            ) {
                // return false
            }
            return $options;
        } else {
                return false;            
        }
    } elseif (\is_integer($options)) {  // param. is a flags bitfield
        return $options;
    } else {
        // failure
    }
}

function filter_var_options($options, $definition) {
    
}
