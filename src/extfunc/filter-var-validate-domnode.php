<?php

namespace ScraperLite;

/*
 * Verifies a DOM node's type and optionally name.
 * 
 * @todo Test units.
 */
function filter_var_validate_domnode(
    \DOMNode $domnode,
    $nodeType = null, // omit to check for a DOMNode type only
    $nodeName = null  // only verifies if given argument 1
) {
    \assert(is_int($nodeType), 'Argument 2 type');
    \assert(is_string($nodeName) or is_null($nodeName), 'Argument 3 type');
    if ((isset($nodeType)
        and $domnode->nodeType === $nodeType 
        and (
            \is_null($nodeName)
            or \strtolower($domnode->nodeName) === \strtolower($nodeName)
    ))) {
        return true;
    } else {
        return false;
    }
}
