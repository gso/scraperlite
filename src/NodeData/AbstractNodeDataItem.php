<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * Instances of subclasses of `AbstractDataItem`, given a XPath expression
 * retrieve data from instances of
 * subclasses of {@link AbstractWebDocument}.
 * A callback can optionally be passed as an argument to queries 
 * enabling processing
 * of the returned data further.
 * (Ref. the class tree  and `AbstractDataItem` subclasses for a list
 * of data types currently implemented.)
 * 
 */
abstract class AbstractNodeDataItem extends AbstractNodeData
{
        
    abstract public function domNode();

    /**
     * @param \ScraperLite\AbstractWebDocument
     * See corresponding class property.
     * @param string|array See corresponding class property.
     * @param DOMNode|AbstractDOMNodeData See corresponding class property.
     */
    public function __construct(
        AbstractWebDocument $webDocument
    ) {
        parent::__construct($webDocument);
    }
    
}
