<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * Retrieve the value of an element attribute.
 * 
 */
class AttributeValue extends AbstractDOMNodeData
{
    
    protected static $NodeType = XML_ATTRIBUTE_NODE;
    protected static $NodeName = null;

    function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }

    /**
     * The value of the attribute.
     * 
     * @return string 
     */
    public function value() {
        return $this->domNode()->nodeValue;
    }

    public function queryResult(callable $callback = null) {
        if (is_null($callback)) {
            return $this->value();
        } else {
            return call_user_func($callback, $this->value());
        }
    }

}
