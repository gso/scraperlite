<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve items from HTML 'ol' or 'ul' lists.
 * The only argument required in additon to the document to be queried 
 * itself being
 *  an XPath expression resolving to the
 * list element.
 *
 * 
 * @todo Check the results returned from nested lists.
 */
class HTMLList extends AbstractList
{

    private $listXPathQuery;
    private $listQueryContext;
    
    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, './/li');
        $this->setListXPathQuery($dataXPathQuery);
        $this->listQueryContext = $queryContext;
    }
    
    public function setListXPathQuery($listXPathQuery) {
        $this->listXPathQuery = $listXPathQuery;
    }
    
    public function listXPathQuery() {
        return $this->listXPathQuery;
    }
    
    public function setListQueryContext($listQueryContext) {
        $this->listQueryContext = $listQueryContext;
    }

    public function listQueryContext() {
        return $this->listQueryContext;
    }
    
    public function listElement() {
        try {            
            $list_element = (
                new DOMNodeData(
                    $this->webDocument(),
                    $this->listXPathQuery(), 
                    $this->listQueryContext()
                )
            );
            $list_domnode = $list_element->domNode();
            if (filter_var_validate_domnode(
                    $list_domnode, 
                    XML_ELEMENT_NODE,
                    'ol'
                ) or filter_var_validate_domnode(
                    $list_domnode, 
                    XML_ELEMENT_NODE,
                    'ul'
            )) {
                return $list_element;
            } else {  // now doc. throws an error with __FUNCTION__ ??
                throw new DataNodeException(
'XPath query must evaluate to an HTML \'ol\' or \'ul\' element in function'
                    . __FUNCTION__ . '.',
                    DATA_QUERY_RESULT_ERR
                );
            }
        } catch (DOMXPathQueryException $ex) {
            throw new DataNodeException(
                'XPath query execution error in function' . __FUNCTION__ . '.'
                . PHP_EOL . $ex->getMessage(),
                $ex->getCode()
            );
        }
    }
    
    public function domNodeList()
    {
            $this->setQueryContext($this->listElement());
            return parent::domNodeList();
    }

}
