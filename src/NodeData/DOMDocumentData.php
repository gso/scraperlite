<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Low level access to a PHP DOM docuement object as returned from a XPath
 * expr.
 * 
 */
class DOMDocumentData extends AbstractDOMNodeListData
{

    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }

    public function domDocument() {
        // Create a new DOMDocument from the query results
        if ($this->length() === 1 and $this->item(0) instanceof \DOMDocument) {
            // query returned an entire document, 
            return $this->item(0);
        } else {
            // create a new DOMDocument from nodes of DOMNodeList
            return domdocument_from_domnodelist($this->domNodeList());
        }
        // Pretty print and output
        $query_result_domdocument->formatOutput = true;
        $response['data'] = htmlentities(
            $query_result_domdocument->saveHTML()
        );
    }
    
    public function source() {
        // Not pretty print (left to caller)
        if ($this->webDocument() instanceof WebPage) {
            return $this->domDocument()->saveHTML();
        }
        elseif ($this->webDocument() instanceof XMLDocument) {
            return $this->domDocument()->saveXML();
        }
        else {
            throw new \LogicException('Document type not implemented');
        }
    }
    
    public function queryResult(callable $callback = null) {
        return $this->source();
    }
}
