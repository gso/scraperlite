<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve the outer XML of an element given in additon to the document 
 * to be queried a XPath expr. resolving to the element.
 */
class ElementOuterXML extends AbstractDOMNodeData
{

    protected static $NodeType = XML_ELEMENT_NODE;
    protected static $NodeName = null;

    function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }
    
    /**
     * Element outer XML.
     * 
     * @return string 
     */
    public function outerXML() {
        return domnode_saveouterhtml($this->domNode());
    }
    
    public function queryResult(callable $callback = null) {
        if (is_null($callback)) {
            return $this->outerXML();
        } else {
            return call_user_func($callback, $this->outerXML());
        }        
    }

}
