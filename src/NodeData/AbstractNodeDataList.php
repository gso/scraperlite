<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * 
 */
abstract class AbstractNodeDataList extends AbstractNodeData 
implements \IteratorAggregate
{

    public function __construct(
        AbstractWebDocument $webDocument
    ) {
        parent::__construct($webDocument);
    }
    
}
