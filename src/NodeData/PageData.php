<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve arbitrary text data from a web page, each item represented by
 * a key in an assoc. array, the value of which a XPath expression retrieving
 * the data.  Text elements and tag attributes supported.
 * 
 */
class PageData extends AbstractNodeDataList //implements \IteratorAggregate
{
    
    private $dataXPathQuery;
    private $queryContext;

    public function __construct(
        AbstractWebDocument $webDocument,
        array $dataXPathQuery,  // an array of XPath expressions
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument);
        $this->setDataXPathQuery($dataXPathQuery);
        $this->setQueryContext($queryContext);
    }
    
    public function setDataXPathQuery($dataXPathQuery) {
        $this->dataXPathQuery = $dataXPathQuery;
    }
        
    public function dataXPathQuery() {
        return $this->dataXPathQuery;
    }
    
    public function setQueryContext($queryContext) {
        $this->queryContext = $queryContext;
    }

    public function queryContext() {
        return $this->queryContext;
    }

    public function fieldDomNodeAry() {
        $field_domnode_ary = [];
        foreach ($this->dataXPathQuery() as $key => $query) {
            assert(is_string($query), "Argument 2 ({$key} key type)");
            $field_domnode_ary[$key] = (
                new DOMNodeData(
                    $this->webDocument(),
                    $query,
                    $this->queryContext()
                )
            )->domNode();
        }
        return $field_domnode_ary;
    }

    /**
     * Returns the data associated with a given field.
     *
     * @param string $key integer|string Field name to retrieve (or integer array offset).
     * @param callable $callback callable See corresponding {@link queryResult()} parameter.
     * @return mixed A value for the requested field, defaulting to the text
     * content of the DOMNode the field XPath expression evaluates to, 
     * or the value returned by a callback function if provided.
     */
    public function field($key, callable $callback = null) {
        assert(array_key_exists($key, $this->fieldDomNodeAry()), 'Argument 1 domain');
        if (is_null($callback)) {
            return $this->fieldDomNodeAry()[$key]->textContent;
        } else {
            return call_user_func(
                $callback, 
                $this, 
                $key, 
                $this->fieldDomNodeAry()[$key]->textContent
            );
        }
    }

    /**
     * Iterates through fields of the record.
     *
     * @param $callback callable See {@link RecordData::getField()}.
     * @return DataRecordIterator
     */
    public function getIterator(callable $callback = null) {
        return new DataRecordIterator(
            $this, 
            $this->fieldDomNodeAry(), 
            $callback
        );
    }

    public function queryResult(callable $callback = null) {
        return iterator_to_array($this->getIterator($callback));
    }
    
}
