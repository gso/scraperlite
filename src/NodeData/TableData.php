<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Operates at DOM node level, retrieving data stored in the format of a table 
 * from two XPath
 * expressions, returning in turn i) rows in the table, and ii) column
 * DOM nodes within each row.  The context node of the column XPath expr. 
 * is that of the row DOM node.
 *
 */
class TableData extends AbstractTable
{
    
    public $columnXPathQuery;
    
    public function __construct(
        AbstractWebDocument $webDocument,
        $rowXPathQuery,
        $columnXPathQuery, // relative to row
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct(
            $webDocument,
            $rowXPathQuery,
            $queryContext
        );
        $this->setColumnXPathQuery($columnXPathQuery);
    }
    
    public function setColumnXPathQuery($fieldXPathQuery) {
        $this->columnXPathQuery = $fieldXPathQuery;
    }

    public function columnXPathQuery() {
        return $this->columnXPathQuery;
    }

    public function rowData(\DOMNode $rowDomNode) {
        return new ListData(
            $this->webDocument(),
            $this->columnXPathQuery(), 
            new DOMNode($this->webDocument(), $rowDomNode) // ScraperLite DOMNode object
        );
    }

}
