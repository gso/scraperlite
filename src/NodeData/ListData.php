<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Operates at DOM node level, retrieving data stored in the format of a list 
 * given in additon to the document to be queried itself 
 * an XPath expr. returning the list's items.
 * 
 */
class ListData extends AbstractList
{

    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }

}
