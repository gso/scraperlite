<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve HTML table data, currently only the body of the table.
 * The only argument required in additon to the document to be queried itself 
 * being an XPath expression resolving to the
 * table element.
 *
 * @todo Table header and footer content methods (subclass, constructor arg, method)
 */
class HTMLTable extends AbstractTable
{

    private $tableXPathQuery;  // read + write
    private $tableQueryContext;  // read + write

    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery,
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct(
            $webDocument,
            'self::table/descendant-or-self::node()[not(name()="th") and not(name()="tfoot")]/tr'
        );
        $this->setTableXPathQuery($dataXPathQuery);
        $this->setTableQueryContext($queryContext);
    }
    
    public function setTableXPathQuery($listXPathQuery) {
        $this->tableXPathQuery = $listXPathQuery;
    }
    
    public function tableXPathQuery() {
        return $this->tableXPathQuery;
    }    
    
    public function setTableQueryContext($queryContext) {
        $this->tableQueryContext = $queryContext;
    }

    public function tableQueryContext() {
        return $this->tableQueryContext;
    }
    
    public function rowData(\DOMNode $rowDomNode) {
        return new ListData(
            $this->webDocument(),
            'self::tr/descendant-or-self::td', 
            new DOMNode($this->webDocument(), $rowDomNode) // ScraperLite DOMNode object
        );
    }

    public function tableElement() {  // HTML 'table' element
        try {            
            $table_element = (
                new DOMNodeData(
                    $this->webDocument(),
                    $this->tableXPathQuery(),
                    $this->tableQueryContext()
                )
            );
            $table_domnode = $table_element->domNode();
            if (filter_var_validate_domnode(
                $table_domnode,
                XML_ELEMENT_NODE,
                'table'
            )) {
                return $table_element;
            } else {
                throw new DataNodeException(
'XPath query must evaluate to an HTML \'table\' element in function '
                    . __FUNCTION__ . '.',
                    DATA_QUERY_RESULT_ERR
                );
            }
        } catch (DOMXPathQueryException $ex) {
            throw new DataNodeException(
                'XPath query execution error in function ' 
                . __FUNCTION__ . '.' . PHP_EOL
                . $ex->getMessage(),
                $ex->getCode()
            );
        }
    }

    public function domNodeList()  // DOMNodeList of table rows
    {
        $this->setQueryContext($this->tableElement());
        return parent::domNodeList();
    }
}
