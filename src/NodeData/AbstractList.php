<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * 
 */
Abstract class AbstractList extends AbstractDOMNodeListData
{
    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct(
            $webDocument,
            $dataXPathQuery,
            $queryContext
        );
    }

    public function item($offset, callable $callback = null) {
        if (is_null($callback)) {
            return parent::item($offset)->textContent;
        } else {
            return parent::item($offset, $callback);
        }
    }

}
