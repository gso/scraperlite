<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal 
 * Iterate through table like structures of DOM nodes.
 *
 * @todo Check results returned from nested tables.
 */
Abstract class AbstractTable extends AbstractDOMNodeListData
{
    
    abstract public function rowData(\DOMNode $rowDomNode);

    /**
     *
     * @param \ScraperLite\AbstractWebDocument $webDocument 
     * @param string $rowXPathQuery XPath expression evaluating to a DOM node representing
     * each row in the table.
     * @param \DOMNode $queryContext see {@see AbstractDataItem::__construct}
     */
    public function __construct(
        AbstractWebDocument $webDocument,
        $rowXPathQuery,
        $queryContext = null
    ) {
        parent::__construct(
            $webDocument,
            $rowXPathQuery,
            $queryContext
        );
    }


    /**
     * Iterates through the rows of the table, returning a
     * {@see ListData} object for each.
     *
     * @return ScraperLite\AbstractDOMNodeListDataIterator
     */
    public function getIterator(
        callable $rowCallback = null,
        callable $colCallback = null
    ) {
        if (is_null($colCallback)) {  // possibly row but not col. callback
            if (is_null($rowCallback)) {
                return parent::getIterator(

                    function (
                        $caller,
                        $rowOffset,
                        \DOMNode $rowDomNode
                    ) {
                        // array of DOMNode objects
                        $row_data = $this->rowData($rowDomNode);
                        return $row_data->queryResult();
                    }

                );
            } else {  // default iterator callback on each table row
                return parent::getIterator($rowCallback);
            }
        } elseif (is_null($rowCallback)) {  // no row, but col. callback
            // callback on individual table cells only (not rows)
            return $this->getIteratorColCallback($colCallback);
        } else {  // row and col. callbacks
            // row callback called with array result of callback on individual
            // table cells
            return $this->getIteratorColRowCallback($rowCallback, $colCallback);
        }
    }

    private function getIteratorColCallback(callable $colCallback) {
        return parent::getIterator(

            // iterator callback on each row
            function (
                $caller,
                $rowOffset,
                \DOMNode $rowDomNode
            ) use ($colCallback) {
                // array of DOMNode objects
                $row_data = $this->rowData($rowDomNode);
                return $row_data->queryResult($colCallback);
            }

        );
    }

    private function getIteratorColRowCallback(
        callable $rowCallback,
        callable $colCallback
    ) {
        return parent::getIterator(

            // iterator callback on each row
            function ($caller, $rowOffset, \DOMNode $rowDomNode)  
            use ($rowCallback, $colCallback) {
                $row_data = $this->rowData($rowDomNode);
                return call_user_func(
                    $rowCallback,
                    $this,
                    $rowOffset,
                    // array of DOMNode objects
                    $row_data->queryResult($colCallback)
                );
            }

        );
    }

    public function queryResult(
        callable $rowCallback = null,
        callable $colCallback = null
    ) {
        return iterator_to_array($this->getIterator(
            $rowCallback,
            $colCallback
        ));
    }

}
