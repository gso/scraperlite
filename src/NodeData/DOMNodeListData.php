<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Low level access to a PHP DOM node list object as returned from a XPath
 * expr.
 * 
 */
class DOMNodeListData extends AbstractDOMNodeListData
{

    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }

    public function item($offset, callable $callback = null) {
        if (is_null($callback)) {
            return parent::item(
                $offset,
                    
                function (
                    AbstractDOMNodeListData $xPathDomNodeList, 
                    $offset, 
                    \DOMNode $domNode
                ) {
                    return $domNode->textContent;
                }
                
            )->textContent;
        } else {
            return parent::item($offset, $callback);
        }
    }

}
