<?php

namespace ScraperLite;  // primitive data types

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * ScraperLite type class wrapping PHP DOM node objects (mainly 
 * facilitating encapsulating
 * the document the DOM node actually belongs to).
 * 
 */
class DOMNode extends AbstractNodeDataItem
{
    
    private $domNode;
    
    public function __construct(
        AbstractWebDocument $webDocument,
        \DOMNode $domNode
    ) {
        parent::__construct($webDocument);
        $this->setDomNode($domNode);
    }
    
    public function setDomNode(\DOMNode $domNode) {
        $this->domNode = $domNode;
    }

    public function domNode() {
        return $this->domNode;
    }

    public function textContent(callable $callback = null) {
        if (is_null($callback)) {
            return $this->domNode()->textContent;
        } else {
            return call_user_func($callback, $this->domNode()->textContent);
        }        
    }

    public function xPathQuery() {
        // TODO memoize (using instance vars, static vars hold state 
        // across instances)
        return $this->domNode()->getNodePath();
    }

}
