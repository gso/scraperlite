<?php

namespace ScraperLite;  // primitive data types

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Low level access to a PHP DOM node object as returned from a XPath
 * expr.
 * 
 */
class DOMNodeData extends AbstractDOMNodeData
{
    
    // default $nodeType & $nodeName (null, any type)

    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }
    
    public function queryResult(callable $callback = null) {
        if (is_null($callback)) {
            return $this->domNode();  //TODO return node value?
        } else {
            return call_user_func($callback, $this->domNode());
        }        
    }

}
