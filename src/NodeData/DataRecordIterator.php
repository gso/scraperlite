<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * 
 */
class DataRecordIterator implements \Iterator
{

    private $recordData;
    private $fieldDomNodeAry;  //TODO factor out
    private $callback;
    private $offset;

    function __construct(PageData $recordData, array $domNodeAry, callable $callback = null) {
        $this->recordData = $recordData;
        $this->fieldDomNodeAry = $domNodeAry;
        $this->callback = $callback;
    }

    function rewind() {
        $this->offset = 0;
    }

    function current() {
        return $this->recordData->field($this->key(), $this->callback);
    }

    function key() {  //TODO memoize keys ary in recordData instance?
        return \array_keys($this->fieldDomNodeAry)[$this->offset];
    }

    function next() {
        $this->offset += 1;
    }

    function valid() {
        return $this->offset < count($this->fieldDomNodeAry);
    }
    
    public function offset() {
        return $this->offset;
    }
    
    public function domNode() {
        return $this->fieldDomNodeAry[$this->key()];
    }
    
    public function recordData() {
        return $this->recordData;
    }

}
