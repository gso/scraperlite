<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

// more than one DOM node returned when expecting a single DOMNode match
const DATA_QUERY_NODE_ERR = 1;  // DOMXPATH_QUERY_NODE_ERR
// ref. query_domxpath_for_domnodelist() - nonvalid query args (runtime err.)
const DATA_QUERY_FAIL_ERR = 2;  // DOMXPATH_QUERY_FAIL_ERR
// expecting a <p> paragraph element, attribute returned, etc.
const DATA_QUERY_RESULT_ERR = 8;

/**
 * @internal
 * 
 */
abstract class AbstractNodeData {
    
    /**
     * @var object An instance of a {@link AbstractWebDocument}
     * subclass.
     */
    private $webDocument;

    function __construct(AbstractWebDocument $webDocument) {
        $this->setWebDocument($webDocument);
    }

    public function setWebDocument($webDocument) {
        $this->webDocument = $webDocument;
    }
    
    public function webDocument() {
        return $this->webDocument;
    }
}
