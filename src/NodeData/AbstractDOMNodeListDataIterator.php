<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * 
 */
class AbstractDOMNodeListDataIterator implements \Iterator
{

    private $iteratorAggregate;
    private $domNodeList;  //TODO factor out
    private $callback;
    private $offset;

    function __construct(
        AbstractDOMNodeListData $iteratorAggregate,  
        \DOMNodeList $domNodeList, 
        callable $callback = null
    ) {
        $this->iteratorAggregate = $iteratorAggregate;
        $this->domNodeList = $domNodeList;
        $this->callback = $callback;
    }

    function rewind() {
        $this->offset = 0;
    }

    function current() {
        return $this->iteratorAggregate->item($this->offset, $this->callback);
    }

    function key() {
        return $this->offset;
    }

    function next() {
        return $this->offset += 1;
    }

    function valid() {
        return $this->offset < $this->domNodeList->length;  // TODO call agg.?
    }
    
    public function offset() {
        return $this->offset;
    }
    
    public function domNode() {
        return $this->domNodeList->item($this->offset);
    }
    
    public function iteratorAggregate() {
        return $this->iteratorAggregate;
    }

}
