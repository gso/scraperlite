<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Analagous to a database table, 
 * each field the name of a key in an assoc. array, the value of 
 * which a XPath expr. evaluated in the context of the record row.
 * Typically could be used to iterate through search engine results.
 * 
 */
class DataRecord extends AbstractTable
{

    private $fieldXPathQuery;

    public function __construct(
        AbstractWebDocument $webDocument,
        $recordXPathQuery,
        array $fieldXPathQuery,
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct(
            $webDocument,
            $recordXPathQuery,
            $queryContext
        );
        $this->fieldXPathQuery = $fieldXPathQuery;
    }
    
    public function setFieldXPathQuery($fieldXPathQuery) {
        $this->fieldXPathQuery = $fieldXPathQuery;
    }

    public function fieldXPathQuery() {
        return $this->fieldXPathQuery;
    }

    public function rowData(\DOMNode $rowDomNode) {
        return new PageData(
            $this->webDocument(),
            $this->fieldXPathQuery(),
            new DOMNode($this->webDocument(), $rowDomNode) // ScraperLite DOMNode
        );
    }

    public function record($offset) {
        assert($this->length() > 0 and filter_var_validate_offset($offset, $this->length() - 1), 'Argument 1 value');
        return $this->rowData(
            $this->domNodeList()->item($offset)
        )->queryResult();  // array
    }
    
}
