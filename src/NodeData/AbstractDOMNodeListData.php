<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * 
 */
abstract class AbstractDOMNodeListData extends AbstractNodeDataList
{

    private $dataXPathQuery;
    private $queryContext;

    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument);
        $this->setDataXPathQuery($dataXPathQuery);
        $this->queryContext = $queryContext;
    }
    
    public function setDataXPathQuery($dataXPathQuery) {
        $this->dataXPathQuery = $dataXPathQuery;
    }
    
    public function dataXPathQuery() {
        return $this->dataXPathQuery;
    }
    
    public function setQueryContext($queryContext) {
        $this->queryContext = $queryContext;
    }

    public function queryContext() {
        return $this->queryContext;
    }
    
    function domNodeList() {
        try {
            return query_domxpath_for_domnodelist(
                $this->webDocument()->domXPath(),
                $this->dataXPathQuery(),  //TODO throws malformed query on empty string (return likewise?, catch remaining?)
                $this->queryContext() !== null  // isset() not valid here r5.4
                ? $this->queryContext()->domNode()
                : null
            );
        } catch (DOMXPathQueryException $ex) {
            throw new DataNodeException(
                'XPath query execution error in function ' 
                . __FUNCTION__ . '.' . PHP_EOL
                . $ex->getMessage(),
                $ex->getCode()
            );
        }
    }

    /**
     * Number of DOMNode instances in the DOMNodeList.
     * 
     * @return integer
     */
    public function length() {
        return $this->domNodeList()->length;
    }

    public function item($offset, callable $callback = null) {
        assert(is_int($offset), 'Argument 1 type');
        assert($this->length() > 0 and filter_var_validate_offset($offset, $this->length() - 1), 'Argument 1 value');
        if (is_null($callback)) {
            return $this->domNodeList()->item($offset);
        } else {
            return call_user_func(
                $callback, 
                $this, 
                $offset,
                $this->domNodeList()->item($offset)
            );
        }
    }

    public function getIterator(callable $callback = null) {  //TODO callback?
        return new AbstractDOMNodeListDataIterator(
            $this, 
            $this->domNodeList(), 
            $callback
        );
    }

    public function queryResult(callable $callback = null) {
        return iterator_to_array($this->getIterator($callback));  //TODO return text content in array?
    }

}
