<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * @internal
 * 
 */
abstract class AbstractDOMNodeData extends AbstractNodeDataItem
{

    abstract protected function queryResult(callable $callback = null);
    
    protected static $NodeType = null;  // overidden by subclasses
    protected static $NodeName = null;  // overidden by subclasses
    private $dataXPathQuery;
    private $queryContext;
    
    
    public function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument);
        $this->dataXPathQuery = $dataXPathQuery;
        $this->queryContext = $queryContext;
    }
    
    public function setDataXPathQuery($dataXPathQuery) {
        $this->dataXPathQuery = $dataXPathQuery;
    }
        
    public function dataXPathQuery() {
        return $this->dataXPathQuery;
    }
    
    public function setQueryContext($queryContext) {
        $this->queryContext = $queryContext;
    }

    public function queryContext() {
        return $this->queryContext;
    }

    public function domNode() {
        try {
            $domnode = query_domxpath_for_domnode(
                $this->webDocument()->domXPath(),
                $this->dataXPathQuery,
                $this->queryContext() !== null  // isset() cannot be used here!
                ? $this->queryContext()->domNode()
                : null
            );
            if ((
                    is_null(static::$NodeType) and $domnode instanceof \DOMNode
                )
                or 
                    filter_var_validate_domnode(
                        $domnode, 
                        static::$NodeType, 
                        static::$NodeName
                    )
            ) {
                return $domnode;
            } else {
                throw new DataNodeException(
                    'XPath query must evaluate to a node of the required type '
                    . 'and name in function ' . __FUNCTION__ . ', '
                    . $domnode->nodeType . ' ' . $domnode->nodeName .  'given.',
                    DATA_QUERY_RESULT_ERR
                );
            }
        } catch (DOMXPathQueryException $ex) {
            throw new DataNodeException(
                'XPath query execution error in function ' 
                . __FUNCTION__ . '.' . PHP_EOL
                . $ex->getMessage(),
                $ex->getCode()
            );
        }
    }

}
