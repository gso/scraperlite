<?php

namespace ScraperLite;

require __DIR__ . \DIRECTORY_SEPARATOR . '..' . \DIRECTORY_SEPARATOR . '..' . 
        \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Retrieve the text content of an element given in additon to the document 
 * to be queried a XPath expr. resolving to the element.
 * 
 */
class ElementText extends AbstractDOMNodeData
{
    
    protected static $NodeType = XML_ELEMENT_NODE;
    protected static $NodeName = null;

    function __construct(
        AbstractWebDocument $webDocument,
        $dataXPathQuery, 
        AbstractNodeDataItem $queryContext = null
    ) {
        parent::__construct($webDocument, $dataXPathQuery, $queryContext);
    }

    /**
     * 
     * @return string Text content of the element and sub-elements.
     */
    public function text() {
        return $this->domNode()->textContent;
    }

    public function queryResult(callable $callback = null) {
        if (is_null($callback)) {
            return $this->text();
        } else {
            return call_user_func($callback, $this->text());
        }        
    }

}
