<?php

/*
 * Build .phar file from 'src' and 'vendor' directories.
 */

// alt. copy contents to staging area (file paths copied into phar)?

//$appRoot = __DIR__;
$appRoot = '.';
$buildRoot = $appRoot . DIRECTORY_SEPARATOR . 'build';
$fname = $buildRoot. DIRECTORY_SEPARATOR . 'scraperlite.phar';
$srcRoot = $appRoot . DIRECTORY_SEPARATOR . 'src';
$indexfile = 'src' . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'cli.php';
$autoloadRoot = $appRoot . DIRECTORY_SEPARATOR . 'vendor';

if (file_exists($fname)) {
    unlink ($fname);
}
$phar = new Phar($fname);
$phar->buildFromDirectory($appRoot, '/^' . preg_quote($srcRoot . 
        DIRECTORY_SEPARATOR, '/') . '/');
$phar->buildFromDirectory($appRoot, '/^' . preg_quote($autoloadRoot . 
        DIRECTORY_SEPARATOR, '/') . '/');
$phar->setStub($phar->createDefaultStub($indexfile));

//$phar->extractTo('/tmp/scraperlite_phar', null, true);
