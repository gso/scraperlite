<?php

require_once $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'config.scraperlite.php';
require_once AUTOLOADER;
use ScraperLite\extfunc;
require_once XPATHUTIL_LIBDIR . 'filter-var-validate-url.php';
require_once XPATHUTIL_LIBDIR . 'curl-fetch.php';
require_once XPATHUTIL_LIBDIR . 'domdocument-factory.php';
require_once XPATHUTIL_LIBDIR . 'domxpath-query.php';

// TODO ...
// - https://github.com/kukulich/fshl syntax highlighter (`class_exists` func.)

const URL_PARAM = 'url';
const QUERY_PARAM = 'query';

session_start();

// false on filter fail, null not exist
$url_param = filter_input(INPUT_POST, URL_PARAM);
$query_param = filter_input(INPUT_POST, QUERY_PARAM);

$response = [];  // Return data and errors as a stringified JSON object
$response['data'] = '';
$response['errors'] = [];

try {

    // URL parameter
    if (!is_null($url_param) and !is_bool($url_param)) {
        // Load the source code
        if (filter_var_validate_url($url_param)) {
            $_SESSION['source'] = curl_fetch(
                $url_param,
                [CURLOPT_PROXY => XPATHUTIL_CURLOPT_PROXY]
            );
        } else {
            throw new \Exception(
                'Parameter' . '\'' . URL_PARAM . '\''
                . ' must by a valid RFC 2396 URL, '
                . $url_param . ' given.'
            );
        }
    }

    // Query parameter
    if (!is_null($query_param) and !is_bool($query_param)) {  // TODO assume html for now
        if (array_key_exists('source', $_SESSION)) {
            // Run an XPath query on the source code
            $domdocument = domdocument_from_html($_SESSION['source']);
            $query_result_domnodelist = query_domxpath_for_domnodelist(
                new DOMXPath($domdocument), 
                $query_param
            );
            // Create a new DOMDocument from the query results
            if (
                $query_result_domnodelist->length === 1
                and $query_result_domnodelist->item(0) instanceof \DOMDocument
            ) {
                // query returned an entire document, 
                $query_result_domdocument = $query_result_domnodelist->item(0);
            } else {
                // create a new DOMDocument from nodes of DOMNodeList
                $query_result_domdocument = domdocument_from_domnodelist(
                    $query_result_domnodelist
                );
            }
            // Pretty print and output
            $query_result_domdocument->formatOutput = true;
            $response['data'] = htmlentities(
                $query_result_domdocument->saveHTML()
            );
        } else {
            throw new \Exception(
                'The source code for the Web page to be queried'
                . 'has not yet been retrieved.  Call the script again with '
                . 'the \'' . URL_PARAM . '\' set.'
            );
        }
    }

} catch (\Exception $ex) {
    //$new_err = count($response['errors']);
    $response['errors'][0]['error'] = get_class($ex);
    $response['errors'][0]['message'] = $ex->getMessage();    
    $response['errors'][0]['code'] = $ex->getCode();
}

echo json_encode($response);
