<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Knowledge_discovery';
$table_xpath_query = '(//table)[1]';  # '[]' binds higher precedence than '//'

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

$table = new ScraperLite\HTMLTable(
        $web_page, 
        $table_xpath_query
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the table from the overview section of the 'Knowledge extraction' Wikipedia page.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'Table XPath query string: ', $table_xpath_query, PHP_EOL;
echo 'Results: ', PHP_EOL;
echo 'Broken, page now redirects to https://en.wikipedia.org/wiki/Data_mining.'
//foreach ($table as $row_data) {
//    echo htmlentities($row_data[0]), ' | ', 
//            htmlentities($row_data[1]), PHP_EOL;
//}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the table from the overview section of the 'Knowledge extraction' Wikipedia page.
Page URL: http://en.wikipedia.org/wiki/Knowledge_discovery
Table XPath query string: //table[1]
Results:
Source | Which data sources are covered: Text, Relational Databases, XML, CSV
Exposition | How is the extracted knowledge made explicit (ontology file, semantic database)? How can you query it?
Synchronization | Is the knowledge extraction process executed once to produce a dump or is the result synchronized with the source? Static or dynamic. Are changes to the result written back (bi-directional)
Reuse of vocabularies | The tool is able to reuse existing vocabularies in the extraction. For example the table column 'firstName' can be mapped to foaf:firstName. Some automatic approaches are not capable of mapping vocab.
Automatisation | The degree to which the extraction is assisted/automated. Manual, GUI, semi-automatic, automatic.
Requires a domain ontology | A pre-existing ontology is needed to map to it. So either a mapping is created or a schema is learned from the source (ontology learning).

 */
