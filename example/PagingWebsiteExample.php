<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$search_url = 'http://www.google.co.uk/search?q=web+scraping';

$search_results = new ScraperLite\PagingWebsite(
    // callback returning the URL for a given page offset of the search results
    function ($search_results, $page_offset) use ($search_url) {
        if ($page_offset < 2) {  // first two pages
            // return the URL of the next page to be retrieved
            return $search_url . '&start=' . $page_offset * 10;
        } else {
            return '';  // empty URL string terminates iterator
        }
    }, 
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the first 2 pages from a Google search with the term 'web scraping'.
<?php

echo 'Results: ', PHP_EOL;
foreach ($search_results as $page_offset => $page_url) {
    echo 'page offset: ', $page_offset, PHP_EOL;
    echo htmlentities('page URL: ' . $search_results->url()), PHP_EOL;
    $result_stats = new ScraperLite\ElementOuterXML(
        $search_results,
        '//div[@id="resultStats"]'
    );
    echo htmlentities(
        '\'resultStats\' outer XML: '
        . $result_stats->outerXML()
        . PHP_EOL
    );
}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the first 2 pages from a Google search with the term 'web scraping'.
Results:
page offset: 0
page URL: http://www.google.co.uk/search?q=web+scraping&start=0
'resultStats' outer XML: <div class="sd" id="resultStats">About 2,430,000 results</div>
page offset: 1
page URL: http://www.google.co.uk/search?q=web+scraping&start=10
'resultStats' outer XML: <div class="sd" id="resultStats">Page 2 of about 2,430,000 results</div>

 */
