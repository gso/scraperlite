<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Web_scraping';
$row_xpath_query = '//div[@id="toc"]//li';
$column_xpath_query = './/span';

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

$table = new ScraperLite\TableData(
    $web_page,
    $row_xpath_query,
    $column_xpath_query
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the table of contents from the 'Web scraping' Wikipedia page.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'Row XPath query string: ', $row_xpath_query, PHP_EOL;
echo 'Relative column XPath query string:', $column_xpath_query, PHP_EOL;
echo 'Results:', PHP_EOL;
foreach ($table as $row_data) {
    echo htmlentities($row_data[0] . ' | ' . $row_data[1] . PHP_EOL);
}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the table of contents from the 'Web scraping' Wikipedia page.
Page URL: http://en.wikipedia.org/wiki/Web_scraping
Row XPath query string: //div[@id="toc"]//li
Relative column XPath query string:.//span
Results:
1 | Techniques
2 | Legal issues
3 | Technical measures to stop bots
4 | References
5 | See also

 */
