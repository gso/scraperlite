<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$search_url = 'http://www.google.co.uk/search?q=web+scraping';

$search_results = new ScraperLite\PagedList(
    // callback returning the URL for a given page offset of the search results
    function ($search_results, $page_offset) use ($search_url) {
        if ($page_offset < 2) {  // first two pages
            // return the URL of the next page to be retrieved
            return $search_url . '&start=' . $page_offset * 10;
        } else {
            return '';  // empty URL string terminates iterator
        }
    }, 
    // callback returning a list iterator for a given page offset
    function ($search_results, $page_offset) {
        // `$search_results` will have the current page at `$page_offset`
        // loaded at this point
        $page_search_results = new ScraperLite\ListData(
                $search_results,
                '//div[@id="res"]//li//h3[1]'  // search result header
        );
        // `(new ArrayIterator($array))->getIterator()` to convert an array
        // to an iterator
        return $page_search_results->getIterator();
    },
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the first 2 pages of result headers from a Google search with the term 'web scraping'.
<?php

echo 'Results: ', PHP_EOL;
$search_results_iter = $search_results->getIterator();
// directly referencing `$search_results_iter` is only necessary inorder to 
// access the value of the 'pageOffset' property, however in a `foreach`
// loop the behaviour of an iterator is identical to the aggregator object
foreach ($search_results_iter as $offset => $search_result) {
    echo htmlentities(
        'Page offset: [' . $search_results_iter->pageOffset . ']'
        . ' Item offset: [' . $offset . ']' 
        . ' | ' . $search_result
        . PHP_EOL
    );
}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the first 2 pages of result headers from a Google search with the term 'web scraping'.
Results:
Page offset: [0] Item offset: [0] | Web scraping - Wikipedia, the free encyclopedia
Page offset: [0] Item offset: [1] | import.io | Free Structured Web Data Scraping Tool
Page offset: [0] Item offset: [2] | Scrapy | An open source web scraping framework for Python
Page offset: [0] Item offset: [3] | Web Scraper - Chrome Web Store - Google
Page offset: [0] Item offset: [4] | Six tools for web scraping - NotProvided.eu
Page offset: [0] Item offset: [5] | Web Scraping Explained - WebHarvy Web Scraper
Page offset: [0] Item offset: [6] | Is web scraping illegal? Depends on what the meaning of the word ...
Page offset: [0] Item offset: [7] | the kimono blog
Page offset: [0] Item offset: [8] | I Don't Need No Stinking API: Web Scraping For Fun and Profit
Page offset: [0] Item offset: [9] | Easy Web Scraping with Python - miguelgrinberg.com
Page offset: [1] Item offset: [0] | Scraping websites using the Scraper extension for Chrome | School ...
Page offset: [1] Item offset: [1] | Scrapinghub | Home
Page offset: [1] Item offset: [2] | Web Scraping and R - R-bloggers
Page offset: [1] Item offset: [3] | Web-Scraping: the Basics | (R news & tutorials) - R-bloggers
Page offset: [1] Item offset: [4] | Kimono Is A Smarter Web Scraper That Lets You API-ify The Web ...
Page offset: [1] Item offset: [5] | Web scraping tutorial - Hackaday
Page offset: [1] Item offset: [6] | Web Scraping 101 with Python - Greg Reda
Page offset: [1] Item offset: [7] | BBC News - Screen scraping: How to profit from your rival's data
Page offset: [1] Item offset: [8] | HTML Scraping  The Hitchhiker's Guide to Python
Page offset: [1] Item offset: [9] | What is Web Scraping? - A Word Definition From the Webopedia ...

 */
