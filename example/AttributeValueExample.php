<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Web_scraping';
$attribute_xpath_query = '//div[@id="catlinks"]//li/a[contains(., "Web scraping")]/@href';

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

$category_uri = new ScraperLite\AttributeValue(
    $web_page,
    $attribute_xpath_query
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the Wikipedia URI for the category of the 'Web scraping' article.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'XPath query string: ', htmlentities($attribute_xpath_query), PHP_EOL;
echo 'Result: ', htmlentities($category_uri->value()), PHP_EOL; 

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the Wikipedia URI for the category of the 'Web scraping' article.
Page URL: http://en.wikipedia.org/wiki/Web_scraping
XPath query string: //div[@id="catlinks"]//li/a[contains(., "Web scraping")]/@href
Result: /wiki/Category:Web_scraping

 */
