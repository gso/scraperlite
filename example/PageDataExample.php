<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Web_scraping';
$field_xpath_query_ary = [ 
    'title' => '//li[@id="cite_note-4"]//a[@class="external text"]',
    'date_accessed' => '//li[@id="cite_note-4"]//span[@class="reference-accessdate"]'
];

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);
        
$citation_record = new ScraperLite\PageData(
    $web_page,
    $field_xpath_query_ary
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the W. Roush web citation from the 'References' section of the 'Web scraping' Wikipedia page.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'Field XPath query strings:', PHP_EOL;
foreach ($field_xpath_query_ary as $field => $xpath_query) {
    echo htmlentities($field . ' => ' . $xpath_query), PHP_EOL;
}
echo 'Results: ', PHP_EOL;
foreach ($citation_record as $field => $data) {
    echo htmlentities($field . ' => ' . $data), PHP_EOL;
}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the W. Roush web citation from the 'References' section of the 'Web scraping' Wikipedia page.
Page URL: http://en.wikipedia.org/wiki/Web_scraping
Field XPath query strings:
title => //li[@id="cite_note-4"]//a[@class="external text"]
date_accessed => //li[@id="cite_note-4"]//span[@class="reference-accessdate"]
Results:
title => "Diffbot Is Using Computer Vision to Reinvent the Semantic Web"
date_accessed => . Retrieved 2013-03-15

 */
