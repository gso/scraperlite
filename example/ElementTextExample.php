<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Web_scraping';
$element_xpath_query = '//li[@id="footer-info-lastmod"]';

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

$page_last_modified = new ScraperLite\ElementText(
    $web_page,
    $element_xpath_query
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the last modified date of a Wikipedia article.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'XPath query string: ', htmlentities($element_xpath_query), PHP_EOL;
echo 'Result: ', htmlentities($page_last_modified->text()), PHP_EOL; 

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the date the Wikipedia 'Web scraping' article was last modified.
Page URL: http://en.wikipedia.org/wiki/Web_scraping
XPath query string: //li[@id="footer-info-lastmod"]
Result: This page was last modified on 16 July 2014 at 21:35.

 */
