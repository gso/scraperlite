<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Web_scraping';
$list_xpath_query = '//h2/span[@id="See_also"]/following::ul[1]';

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);
        
$see_also_list_items = new ScraperLite\HTMLList(
        $web_page,
        $list_xpath_query
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the 'See also' links from a Wikipedia page.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'XPath query string: ', htmlentities($list_xpath_query), PHP_EOL;
echo 'Results:', PHP_EOL;
foreach ($see_also_list_items as $item) {
    echo htmlentities($item), PHP_EOL;
}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the 'See also' links from a Wikipedia page.
Page URL: http://en.wikipedia.org/wiki/Web_scraping
XPath query string: //h2/span[@id="See_also"]/following::ul[1]
Results:
data scraping
knowledge discovery

 */
