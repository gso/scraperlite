<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . 
        DIRECTORY_SEPARATOR . 'autoload.php';

$web_page_url = 'http://en.wikipedia.org/wiki/Web_scraping';
$records_xpath_query = '//span[@id="See_also"]/following::ul[1]/li';
$field_xpath_query_ary = [ 
    'page_title' => './a/@title',
    'wikipedia_uri' => './a/@href'
];

$web_page = new ScraperLite\WebPage(
    $web_page_url,
    [ CURLOPT_FOLLOWLOCATION => TRUE ]  // page location moved (curl --location)
);

$see_also_links = new ScraperLite\DataRecord(
    $web_page,
    $records_xpath_query,
    $field_xpath_query_ary
);

?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <pre>
Retrieve the page title and Wikipedia URI of each link in the 'See also' section of the 'Web scraping' Wikipedia page.
<?php

echo 'Page URL: ', htmlentities($web_page_url), PHP_EOL;
echo 'Record XPath query string: ', $records_xpath_query, PHP_EOL;
echo 'Relative field XPath query strings:', PHP_EOL;
foreach ($field_xpath_query_ary as $field => $data) {
    echo htmlentities($field . ' => ' . $data), PHP_EOL;
}
echo 'Results:', PHP_EOL;
foreach ($see_also_links as $see_also_item_data) {
    foreach ($see_also_item_data as $field => $data) {
        echo htmlentities($field . ' => ' . $data . PHP_EOL);
    }
}

?>
    </pre>
    </body>
</html>
<?php

/* OUTPUT

Retrieve the page title and Wikipedia URI of each link in the 'See also' section of the 'Web scraping' Wikipedia page.
Page URL: http://en.wikipedia.org/wiki/Web_scraping
Record XPath query string: //span[@id="See_also"]/following::ul[1]/li
Relative field XPath query strings:
page_title => ./a/@title
wikipedia_uri => ./a/@href
Results:
page_title => Data scraping
wikipedia_uri => /wiki/Data_scraping
page_title => Knowledge discovery
wikipedia_uri => /wiki/Knowledge_discovery

 */
