<?php

namespace ScraperLite;

/**
 * PHPUnit test data configuration
 */

/**
 * Development server test data URL (dummy data fetched by curl)
 * 
 * php.ini: xdebug.profiler_enable=On  # code profiling
 */
const TEST_DATA_URL = 'http://localhost/scraperlite/test/data/';

// check php.ini settings