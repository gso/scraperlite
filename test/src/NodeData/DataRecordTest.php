<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class DataRecordTest extends \PHPUnit_Framework_TestCase
{

    public $object;
    
    public $data_title_ary = [
        'Dolor Sit Amet Consectetuer',
        'Ligula Eget Dolor',
        'Cum Sociis Natoque Penatibus'
    ];
    
    public $data_url_ary = [
        'http://lorem.ipsum.com',
        'http://aenean.commodo.com',
        'http://aenean.massa.com'
    ];

    public function setUp() {
        $this->object = new ScraperLite\DataRecord(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '//ol[@id="record-list"]/li',  // record
            [
                'title' => './a',  // fields
                'url' => './a/@href' 
            ]
        );
    }

    public function testDataRecord() {
        $this->assertInstanceOf(
            'ScraperLite\DataRecord', 
            $this->object
        );
    }

    // TODO rowData()

    public function testRecord() {
        $this->expectOutputString('Dolor Sit Amet Consectetuer');
        print $this->object->record(0)['title'];
    }

}
