<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class HTMLTableTest extends \PHPUnit_Framework_TestCase
{

    public $object;  // HTMLTable

    public function setUp() {
        $this->object = new ScraperLite\HTMLTable(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '(//table)[1]'
        );
    }
    
    // class methods
    
    function testHTMLTable() {
        $this->assertInstanceOf(
            'ScraperLite\HTMLTable', 
            $this->object
        );
    }

    public function testTableElement() {
        $table_element = $this->object->tableElement();
        $this->assertInstanceOf(
            'ScraperLite\DOMNodeData', 
            $table_element
        );
        $this->assertSame($table_element->domNode()->nodeName, 'table');
    }
    
    public function test2TableElement() {
        // - not a HTML list element
        $this->setExpectedException(
            'ScraperLite\DataException', 
            null,
            ScraperLite\DATA_QUERY_RESULT_ERR
        );
        $query = $this->object->tableXPathQuery();
        $this->object->setTableXPathQuery('(//p)[1]');
        $this->object->tableElement();  // XPath should evaluate to a table node
        $this->object->setTableXPathQuery($query);
    }
    
    public function test3TableElement() {
        // - multiple DOM nodes returned by query
        // - invalid query string (not tested)
        $this->setExpectedException(
            'ScraperLite\DataException',
            null,
            ScraperLite\DATA_QUERY_NODE_ERR
        );
        $query = $this->object->tableXPathQuery();
        $this->object->setTableXPathQuery('//p');
        $this->object->tableElement();  // XPath should evaluate to a table DOM node
        $this->object->setTableXPathQuery($query);
    }
    
    public function testDomNodeList() {
        $dom_node_list = $this->object->domNodeList();
        $this->assertInstanceOf('\DOMNodeList', $dom_node_list);
    }
    
}
