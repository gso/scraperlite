<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class ListDataTest extends \PHPUnit_Framework_TestCase
{

    public $object;
    
    public $data_ary = [
        'Lorem ipsum dolor sit amet consectetuer.',
        'Aenean commodo ligula eget dolor.',
        'Aenean massa cum sociis natoque penatibus.'
    ];

    public function setUp() {
        $this->object = new ScraperLite\ListData(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '(//ul)[1]/li'
        );
    }
    
    // AbstractXPathDOMNodeList supersuperclass methods
    
    public function testDomNodeList() {
        $dom_node_list = $this->object->domNodeList();
        $this->assertInstanceOf('\DOMNodeList', $dom_node_list);
    }
    
    public function testLength() {
        $this->assertEquals($this->object->length(), 3);
    }
    
    // getIterator(), queryResult()

    public function testGetIterator() {
        $result_ary = [];
        foreach ($this->object as $value) {
            $result_ary[] = $value;
        }
        $this->assertEmpty(array_diff($this->data_ary, $result_ary));
    }
    
    public function testQueryResult() {
        $this->assertEmpty(array_diff(
            $this->data_ary, 
            $this->object->queryResult()
        ));
    }
    
    // AbstractList superclass methods
    
    function testItem() {
        $this->assertSame(
            $this->object->item(1),
            'Aenean commodo ligula eget dolor.'
        );
    }
    
    // class methods

    public function testListData() {
        $this->assertInstanceOf('ScraperLite\ListData', $this->object);
    }
    
}
