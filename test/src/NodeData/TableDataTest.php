<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class TableDataTest extends \PHPUnit_Framework_TestCase
{

    public $object;

    public $col1_data = [
        'Lorem Ipsum',
        'Aenean Commodo',
        'Aenean Massa'
    ];
    
    public $col2_data = [
        'dolor sit amet consectetuer',
        'ligula eget dolor',
        'cum sociis natoque penatibus'
    ];

    public function setUp() {
        $this->object = new ScraperLite\TableData(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '//ol[@id="table"]/li',  // row
            './/span'  // col
        );
    }

    // AbstractXPathDOMNodeList supersuperclass methods

    public function testDomNodeList() {
        $dom_node_list = $this->object->domNodeList();
        $this->assertInstanceOf('\DOMNodeList', $dom_node_list);
    }
    
    public function testLength() {
        $this->assertEquals($this->object->length(), 3);
    }
        
    // AbstractTable superclass methods
    
    public function testGetIterator() {
        $col1_result = [];
        $col2_result = [];
        $count = 0;
        foreach ($this->object as $row_data_ary) {
            $col1_result[] = $row_data_ary[0];
            $col2_result[] = $row_data_ary[1];
            $count += 1;
        }
        $this->assertEquals($count, 3);
        $this->assertEmpty(array_diff($this->col1_data, $col1_result));
        $this->assertEmpty(array_diff($this->col2_data, $col2_result));
    }
    
    function testQueryResult() {  //TODO test with callbacks
        $result_ary = $this->object->queryResult();
        $col1_result = [];
        $col2_result = [];
        foreach ($result_ary as $row_data_ary) {
            $col1_result[] = $row_data_ary[0];
            $col2_result[] = $row_data_ary[1];
        }
        $this->assertCount(3, $result_ary);
        $this->assertEmpty(array_diff($this->col1_data, $col1_result));
        $this->assertEmpty(array_diff($this->col2_data, $col2_result));
    }
    
    // class methods
    
    public function testTableData() {
        $this->assertInstanceOf('ScraperLite\TableData', $this->object);
    }
    
}
