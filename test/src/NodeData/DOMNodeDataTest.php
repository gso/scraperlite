<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class DOMNodeDataTest extends \PHPUnit_Framework_TestCase
{

    protected $object;

    protected function setUp() {
        $this->object = new ScraperLite\DOMNodeData(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '//p[@id="xpath-domnode"]'
        );
    }

    public function testXPathDOMNode() {
        $this->assertInstanceOf(
            'ScraperLite\DOMNodeData', 
            $this->object
        );
    }

}
