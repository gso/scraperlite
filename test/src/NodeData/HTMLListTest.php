<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class HTMLListTest extends \PHPUnit_Framework_TestCase
{

    protected $object;

    protected function setUp() {
        $this->object = new ScraperLite\HTMLList(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '(//ul)[1]'
        );
    }
    
    // class methods
    
    public function testHTMLList() {
        $this->assertInstanceOf(
            'ScraperLite\HTMLList', 
            $this->object
        );
    }

    public function testListElement() {
        $list_element = $this->object->listElement();
        $this->assertInstanceOf(
            'ScraperLite\DOMNodeData', 
            $list_element
        );
        $this->assertSame($list_element->domNode()->nodeName, 'ul');
    }
    
    public function test2ListElement() {
        // - not a HTML list element
        $this->setExpectedException(
            'ScraperLite\DataException', 
            null,
            ScraperLite\DATA_QUERY_RESULT_ERR
        );
        $query = $this->object->listXPathQuery();
        $this->object->setListXPathQuery('(//p)[1]');
        $this->object->listElement();  // XPath should evaluate to a table node
        $this->object->setListXPathQuery($query);
    }
    
    public function test3ListElement() {
        // - multiple DOM nodes returned by query
        // - invalid query string (not tested)
        $this->setExpectedException(
            'ScraperLite\DataException',
            null,
            ScraperLite\DATA_QUERY_NODE_ERR
        );
        $query = $this->object->listXPathQuery();
        $this->object->setListXPathQuery('//p');
        $this->object->listElement();  // XPath should evaluate to a table DOM node
        $this->object->setListXPathQuery($query);
    }
    
    public function testDomNodeList() {
        $dom_node_list = $this->object->domNodeList();
        $this->assertInstanceOf('\DOMNodeList', $dom_node_list);
    }

}
