<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class DOMDocumentDataTest extends \PHPUnit_Framework_TestCase
{

    protected $object;

    protected function setUp() {
        $this->object = new ScraperLite\DOMDocumentData(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '//h2'  // 3 x level 2 headings 
        );
    }

    public function testXPathDOMDocument() {
        $this->assertInstanceOf(
            'ScraperLite\DOMDocumentData', 
            $this->object
        );
    }
    
    public function testQueryResult() {
        $this->assertInstanceOf(
            'DOMDocument', 
            $this->object->domDocument()
        );
    }

}
