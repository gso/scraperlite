<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class AttributeValueTest extends \PHPUnit_Framework_TestCase
{
    public $object;
    
    public function setUp() {
        // Load test data from localhost webserver
        $this->object = new ScraperLite\AttributeValue(
            new ScraperLite\WebPage(
               ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '//table/@class'
        );
    }
    
    // AbstractXPathDOMNode superclass methods
    
    public function testDomNode() {
        $this->assertInstanceOf(
            '\DOMNode',
            $this->object->domNode());
    }

    public function test2DomNode() {
        $this->setExpectedException(
            'ScraperLite\DataNodeException', 
            null, 
            ScraperLite\DATA_QUERY_RESULT_ERR
        );
        $query = $this->object->dataXPathQuery();
        $this->object->setDataXPathQuery('//table');  // invalid (element) node
        $this->object->domNode();
        $this->object->setDataXPathQuery($query);
    }

    public function test3DomNode() {
        $this->setExpectedException(
            'ScraperLite\DataNodeException',
            null,
            ScraperLite\DATA_QUERY_NODE_ERR
        );
        $query = $this->object->dataXPathQuery();
        $this->object->setDataXPathQuery('//p');  // match more than one node
        $this->object->domNode();
        $this->object->setDataXPathQuery($query);
    }
    
    // Invalid XPath expression test
    // - PHPUnit overides any thrown exception with an invalid xpath 
    // expression warning for some odd reason.
    
    // class methods

    public function testValue() {
        $this->expectOutputString('data');
        print $this->object->value();
    }

    public function testQueryResult() {   //TODO call with callback
        $this->expectOutputString('data');
        print $this->object->value();        
    }
    
}
