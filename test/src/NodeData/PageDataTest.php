<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class PageDataTest extends \PHPUnit_Framework_TestCase
{

    protected $object;
    
    public $data_ary = [
        'title' => 'Dolor Sit Amet Consectetuer',
        'url' => 'http://lorem.ipsum.com'
    ];

    protected function setUp() {
        $this->object = new ScraperLite\PageData(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
//            '//ol[@id="record-list"]',
            [ 
//                'title' => '(.//li)[1]/a',
//                'url' => '(.//li)[1]/a/@href' 
                'title' => '(//ol[@id="record-list"]/li)[1]/a',
                'url' => '(//ol[@id="record-list"]/li)[1]/a/@href' 
            ]
        );
    }

    public function testDataRecord() {
        $this->assertInstanceOf('ScraperLite\PageData', $this->object);
    }
    
//    public function testField() {
//        
//    }
    
    public function testGetIterator() {
        $results_ary = [];
        foreach ($this->object as $key => $value) {
            $results_ary[$key] = $value;
        }
        $this->assertEmpty(
            array_diff_assoc($this->data_ary, $results_ary)
        );
    }

//    public function testGetIteratorCallback() {
//        $iterator = $this->object->getIterator(
//            function ($caller, $offset, $textContent) {
//                return [$offset, $textContent];
//            }
//        );
//    }

}
