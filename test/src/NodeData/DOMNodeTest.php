<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class DOMNodeTest extends \PHPUnit_Framework_TestCase
{
    public $object;
    
    public function setUp() {
        $doc = new ScraperLite\WebPage(
            ScraperLite\TEST_DATA_URL . 'test_page.html',
            []
        );
        $el = new ScraperLite\ElementText( $doc, '//h1[@id="domnode"]');
        $this->object = new ScraperLite\DOMNode($doc, $el->domNode());
    }    
    
    public function testTextContent() {
        $this->assertSame(
            $this->object->textContent(), 
            'Lorem ipsum dolor sit amet consectetuer adipiscing elit'
        );
    }
    
    public function testXPathQuery() {
        $this->assertSame(
            $this->object->xPathQuery(), 
            '/html/body/h1'
        );        
    }
}
