<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class ElementTextTest extends \PHPUnit_Framework_TestCase
{

    protected $object;

    protected function setUp() {
        $this->object = new ScraperLite\ElementText(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ), 
            '//h1[1]'
        );
    }

    public function testHtmlElementText() {
        $this->expectOutputString(
            'Lorem ipsum dolor sit amet consectetuer adipiscing elit'
        );
        print $this->object->text();
    }

}
