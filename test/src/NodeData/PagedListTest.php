<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class PagedListTest extends \PHPUnit_Framework_TestCase
{

    protected $object;  // PagedList

    protected function setUp() {
        $this->object = new ScraperLite\PagedList(
            function ($paged_list, $page_offset) {  // page URL
                if ($page_offset < 3) {  // 3 pages in total
                    // return the URL of the next page to be retrieved
                    return ScraperLite\TEST_DATA_URL 
                        . 'pagedlist_' . strval($page_offset + 1) . '.html';
                } else {
                    return '';  // empty URL string terminates iterator
                }
            },
            function ($paged_list) {  // list items
                $table_data = new ScraperLite\HTMLTable(
                        $paged_list,
                        '//table'
                );
                return $table_data->getIterator();
            },
            [ CURLOPT_PROXY => '' ]  // localhost (bypassing env. proxy)            
        );
    }
    
    public function testPageUrlAtOffset() {
        $this->expectOutputString(
            ScraperLite\TEST_DATA_URL . 'pagedlist_2.html'
        );
        $page_url = $this->object->pageUrlAtOffset(1);
        print $page_url;
    }
    
    public function testLoadPageAtOffset() {
        $this->object->loadPageAtOffset(1);
        $this->expectOutputString(
            ScraperLite\TEST_DATA_URL . 'pagedlist_2.html'
        );
        print $this->object->url();
    }

    public function testGetIterator() {
        $data_ary = [
            'Entry First Line 1', 
            'Entry First Line 2',
            'Entry First Line 3',
            'Entry First Line 4',
            'Entry Line 1',
            'Entry Line 2',
            'Entry Line 3',
            'Entry Line 4',
            'Entry Last Line 1',
            'Entry Last Line 2',
            'Entry Last Line 3',
            'Entry Last Line 4',
            'Entry First Line 1',
            'Entry First Line 2',
            'Entry First Line 3',
            'Entry First Line 4',
            'Entry Line 1',
            'Entry Line 2',
            'Entry Line 3',
            'Entry Line 4',
            'Entry Last Line 1',
            'Entry Last Line 2',
            'Entry Last Line 3',
            'Entry Last Line 4',
            'Entry First Line 1',
            'Entry First Line 2',
            'Entry First Line 3',
            'Entry First Line 4',
            'Entry Line 1',
            'Entry Line 2',
            'Entry Line 3',
            'Entry Line 4',
            'Entry Last Line 1',
            'Entry Last Line 2',
            'Entry Last Line 3',
            'Entry Last Line 4'
        ];
        $result_ary = [];
        foreach ($this->object as $row_data) {
            foreach ($row_data as $data_item) {
                $result_ary[] = $data_item;            
            }
        }
        $this->assertEquals(count($result_ary), 12 * 3);
        $this->assertEmpty(\array_diff($data_ary, $result_ary));
    }
   
}
