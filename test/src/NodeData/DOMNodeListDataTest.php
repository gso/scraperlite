<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class DOMNodeListDataTest extends \PHPUnit_Framework_TestCase
{

    protected $object;

    protected function setUp() {
        $this->object = new ScraperLite\DOMNodeListData(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '//h2'
        );
    }

    public function testXPathDOMNodeList() {
        $this->assertInstanceOf(
            'ScraperLite\DOMNodeListData', 
            $this->object
        );
    }

}
