<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class ElementOuterXMLTest extends \PHPUnit_Framework_TestCase
{

    protected $object;

    protected function setUp() {
        $this->object = new ScraperLite\ElementOuterXML(
            new ScraperLite\WebPage(
                ScraperLite\TEST_DATA_URL . 'test_page.html',
                []
            ),
            '(//a)[@class="external ext"]'
        );
    }

    public function testOuterHtml() {
        $this->expectOutputString('<a class="external ext" href="#">link</a>');
        print $this->object->outerXML();
    }

}
