<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class domdocument_factoryTest extends \PHPUnit_Framework_TestCase
{
    
    private $html = <<<'EOD'
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>TODO write content</div>
    </body>
</html>
EOD;
    private $invalid_html = <<<'EOD'
<!DOCTYPE htl>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    </body>
        <div>TODO write content</div>
</html>
EOD;
    private $xml = <<<'EOD'
<?xml version="1.0" encoding="UTF-8"?>
<note>
    <to> Tove</to>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>
EOD;
    private $invalid_xml = <<<'EOD'
<?xm version="1.0" encoding="UTF-8"?>
<note>
    <to> Tove</too>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>
EOD;
        
//    public function test_domdocument_from_html() {
//        // Invalid HTML.
//        // - seems more or less impossible to knock the HTML parser over,
//        // skip for now
//        $this->setExpectedException('InvalidArgumentException');
//        domdocument_from_html($this->invalid_html);
//    }
    
    public function test2_domdocument_from_html() {
        $domdocument = ScraperLite\domdocument_from_html($this->html);
        $this->assertInstanceOf('DOMDocument', $domdocument);
        return $domdocument;
    }

    public function test_domdocument_from_xml() {
        $this->assertInstanceOf(
            'DOMDocument',
            ScraperLite\domdocument_from_xml($this->xml)
        );
    }
    
    /**
     * @depends test2_domdocument_from_html
     */
    public function test_domdocument_from_domnode(\DOMDocument $domdocument) {
        $this->assertInstanceOf(
            'DOMDocument',
            ScraperLite\domdocument_from_domnode(
                $domdocument->getElementsByTagName('title')->item(0)
            )
        );
    }

    /**
     * @depends test_domdocument_from_html
     */
//    public function test_domdocument_from_domnodelist(\DOMDocument $domdocument) {
//        $this->assertInstanceOf(
//            'DOMDocument',
//            domdocument_from_domnodelist(
//                $domdocument->getElementsByTagName('meta')
//            )
//        );
//    }

}
