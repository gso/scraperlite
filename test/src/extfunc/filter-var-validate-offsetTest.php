<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class filter_var_validate_offsetTest extends \PHPUnit_Framework_TestCase
{    
    public function test_filter_var_validate_offset() {
        $this->assertTrue(ScraperLite\filter_var_validate_offset(1));
        $this->assertTrue(ScraperLite\filter_var_validate_offset(1, 2));
    }

    public function test2_filter_var_validate_offset() {
        // upper and lower bounds
        $this->assertFalse(ScraperLite\filter_var_validate_offset(-1));
        $this->assertFalse(ScraperLite\filter_var_validate_offset(2, 1));
    }

}
