<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class curl_fetchTest extends \PHPUnit_Framework_TestCase
{
    
    public function test_curl_fetch() {
        $document = ScraperLite\curl_fetch(
                ScraperLite\TEST_DATA_URL . 'test_page.html', 
                []
        );
        $this->assertTrue(is_string($document));
        $this->assertNotEmpty($document);
    }
}
