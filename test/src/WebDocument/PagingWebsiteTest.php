<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class PagingWebsiteTest extends \PHPUnit_Framework_TestCase
{

    public $object;  // PagingWebsite

    public $data_ary = [];
    
    public function setUp() {
        $this->object = new ScraperLite\PagingWebsite(
            function ($paging_website, $page_offset) {  // page URL callback
                if ($page_offset < 3) {  // 3 pages in total
                    // return the URL of the next page to be retrieved
                    return ScraperLite\TEST_DATA_URL 
                        . 'pagedlist_' . strval($page_offset + 1) . '.html';
                } else {
                    return '';  // empty URL string terminates iterator
                }
            },
            [ CURLOPT_PROXY => '' ]  // localhost (bypassing env. proxy)                    
        );
        $data_ary[] = 
            ScraperLite\TEST_DATA_URL . 'pagedlist_' . strval(1) . '.html';
        $data_ary[] = 
            ScraperLite\TEST_DATA_URL . 'pagedlist_' . strval(2) . '.html';
        $data_ary[] = 
            ScraperLite\TEST_DATA_URL . 'pagedlist_' . strval(3) . '.html';
    }
    
    public function testPageUrlAtOffset() {
        $this->expectOutputString(
            ScraperLite\TEST_DATA_URL . 'pagedlist_2.html'
        );
        $page_url = $this->object->pageUrlAtOffset(1);
        print $page_url;
    }
    
    public function testLoadPageAtOffset() {
        $this->object->loadPageAtOffset(1);
        $this->expectOutputString(
            ScraperLite\TEST_DATA_URL . 'pagedlist_2.html'
        );
        print $this->object->url();
    }

    public function testPagingWebsiteIterator() {
        $result_ary = [];
        foreach ($this->object as $pageUrl) {
            $result_ary[] = $pageUrl;
        }
        $this->assertEmpty(array_diff($this->data_ary, $result_ary));
    }

}
