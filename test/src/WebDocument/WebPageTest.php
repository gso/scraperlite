<?php

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'test' . DIRECTORY_SEPARATOR . 'config.php';

class WebPageTest extends \PHPUnit_Framework_TestCase
{

    protected $object;  // WebPage

    protected function setUp() {
        $this->object = new ScraperLite\WebPage(
            ScraperLite\TEST_DATA_URL . 'test_page.html',
            []
        );
    }

    public function testWebPage() {
        $this->assertInstanceOf(
            'ScraperLite\WebPage', 
            $this->object
        );
    }

}
