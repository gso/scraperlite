var class_scraper_lite_1_1_page_data =
[
    [ "__construct", "class_scraper_lite_1_1_page_data.html#abc0d2034d4fdcef2f10350d9f198f13c", null ],
    [ "dataXPathQuery", "class_scraper_lite_1_1_page_data.html#aa3b171892a8391954dc6823269e2972e", null ],
    [ "field", "class_scraper_lite_1_1_page_data.html#a800b9e93999fda9a776668adacef95a9", null ],
    [ "fieldDomNodeAry", "class_scraper_lite_1_1_page_data.html#af2ac240423caa51661b6d7bf6a326d3a", null ],
    [ "getIterator", "class_scraper_lite_1_1_page_data.html#adb51ff85d44996e22fc98b9cb36f2ab8", null ],
    [ "queryContext", "class_scraper_lite_1_1_page_data.html#a4a0b0eea7e95aa948b5d9531797bc9cb", null ],
    [ "queryResult", "class_scraper_lite_1_1_page_data.html#ad7a6f83e7c5f9ff827fb5007772ab412", null ],
    [ "setDataXPathQuery", "class_scraper_lite_1_1_page_data.html#a8ad4294a33c8cbd580d507b58856b550", null ],
    [ "setQueryContext", "class_scraper_lite_1_1_page_data.html#a0a0adfe83319ac4bb27e54095f1a3b12", null ]
];