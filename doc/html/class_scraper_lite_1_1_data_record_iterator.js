var class_scraper_lite_1_1_data_record_iterator =
[
    [ "__construct", "class_scraper_lite_1_1_data_record_iterator.html#ae9968d68830a6d6f9a9bfd593f3b17e2", null ],
    [ "current", "class_scraper_lite_1_1_data_record_iterator.html#af343507d1926e6ecf964625d41db528c", null ],
    [ "domNode", "class_scraper_lite_1_1_data_record_iterator.html#a68aa64d464445d6afd60efe3d0be1fd9", null ],
    [ "key", "class_scraper_lite_1_1_data_record_iterator.html#a729e946b4ef600e71740113c6d4332c0", null ],
    [ "next", "class_scraper_lite_1_1_data_record_iterator.html#acea62048bfee7b3cd80ed446c86fb78a", null ],
    [ "offset", "class_scraper_lite_1_1_data_record_iterator.html#aae1d15eaade05a1b7067601ccf862d6d", null ],
    [ "recordData", "class_scraper_lite_1_1_data_record_iterator.html#ac66825546dbc7bddf8be1b98fac22e9f", null ],
    [ "rewind", "class_scraper_lite_1_1_data_record_iterator.html#ae619dcf2218c21549cb65d875bbc6c9c", null ],
    [ "valid", "class_scraper_lite_1_1_data_record_iterator.html#abb9f0d6adf1eb9b3b55712056861a247", null ]
];