# ScraperLite (pre-alpha)

Light weight PHP 5.6 web scraper utilising 
[`libcurl`](http://curl.haxx.se/) to fetch pages and 
[`DOMXPath`](http://php.net/manual/en/class.domxpath.php) to extract data:

* Utilities for testing XPath expressions _[limited but functional CLI in place]_
* Utilities for analysing query strings _[TODO]_
* Custom data classes extending existing classes (subclassing, traits, config. file, etc.), the idea being to create packages of classes that will scrape a particular site _[TODO]_
* Fully tested _[TODO]_
* Fully documented _[API documentation and tutorial in progress]_
* Coded with SOLID OOP and design principles, and to php-fig standards _[in progress]_

### Document Classes

These classes retrieve a web site, web page(s) or a document otherwise.

[**WebPage**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_web_page.html) — vanilla web page class ([example](https://bitbucket.org/gso/scraperlite/src/master/example/ElementTextExample.php))  
[**PagingWebsite**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_paging_website.html) — iterate through the pages of a paging web site ([example](https://bitbucket.org/gso/scraperlite/src/master/example/PagingWebsiteExample.php))  
[**PagedList**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_paged_list.html) — iterate through search engine results and other list data that is paged ([example](https://bitbucket.org/gso/scraperlite/src/master/example/PagedListExample.php))  
[**XMLDocument**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_x_m_l_document.html) — load a XML document  

PHP [cURL session options](http://php.net/manual/en/function.curl-setopt.php) reference.  
Malformed HTML should load successfully ([DOMDocument::loadHTML](http://php.net/manual/en/domdocument.loadhtml.php)).  

###Data Classes

These classes given a XPath expression return data from instances of document classes.

[**ElementText**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_element_text.html) — text content of a XML or HTML element ([example](https://bitbucket.org/gso/scraperlite/src/master/example/ElementTextExample.php))  
[**AttributeValue**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_attribute_value.html) — element attribute value ([example](https://bitbucket.org/gso/scraperlite/src/master/example/AttributeValueExample.php))  
[**HTMLList**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_h_t_m_l_list.html) — iterate through HTML ordered and unordered lists ([example](https://bitbucket.org/gso/scraperlite/src/master/example/HTMLListExample.php))  
[**HTMLDescriptionList**]() — _[TODO]_  
[**HTMLTable**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_h_t_m_l_table.html) — iterate through the rows of an HTML table ([example](https://bitbucket.org/gso/scraperlite/src/master/example/HTMLTableExample.php))  
[**ListData**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_list_data.html) — iterate through DOM nodes returned by an XPath expression - a generic version of HTMLList ([example](https://bitbucket.org/gso/scraperlite/src/master/example/ListDataExample.php))  
[**TableData**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_table_data.html) — a generic version of HTMLTable, rows and columns can be any type of DOM node ([example](https://bitbucket.org/gso/scraperlite/src/master/example/TableDataExample.php))  
[**PageData**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_page_data.html) — arbitrary text data located on a web page, each item an assoc. array key, the value of which a XPath expression retrieving the data for that item ([example](https://bitbucket.org/gso/scraperlite/src/master/example/PageDataExample.php))  
[**DataRecord**](http://gso.bitbucket.io/ScraperLite/html/class_scraper_lite_1_1_data_record.html) — mimicking records in a database, retrieving each record in turn (typically, e.g., search engine result listings), each field name is the key of an assoc. array, the value of which a XPath expression retrieving the field data ([example](https://bitbucket.org/gso/scraperlite/src/master/example/DataRecordExample.php))  

PHP [DOMXPath](http://au1.php.net/manual/en/class.domxpath.php) currently supports [XPath 1.0](http://www.w3.org/TR/xpath).

##Utilities

Command line script usage, currently limited capability however demonstrates the concept:
```
php src\cli\cli.php
```
Retrieve web page source:
```
php src\cli\cli.php http://lipsum.com
```
Run a XPath query on a web page:
```
php src\cli\cli.php http://lipsum.com  "//h2"
```
Retrieve the text content of an HTML element:
```
php src\cli\cli.php -n element http://lipsum.com  "/html/head/title" 
```
Retrieve an attribute value:
```
php src\cli\cli.php -n attribute http://www.blueprintcss.org/tests/parts/elements.html "/html/@lang"
```
Retrieve the text content of an HTML list:
```
php src\cli\cli.php -n htmllist http://www.blueprintcss.org/tests/parts/elements.html "(//ol)[1]"
```
Retrieve the text content of an HTML table:
```
php src\cli\cli.php -n htmltable http://www.blueprintcss.org/tests/parts/elements.html "(//table)[2]"
```

##Developers

git clone `https://bitbucket.org/gso/scraperlite.git`.  
Project setup [readme](https://bitbucket.org/gso/scraperlite/src/master/readme).

##Documentation

Code [examples](https://bitbucket.org/gso/scraperlite/src/master/example/).  
API [documentation](http://gso.bitbucket.io/ScraperLite/html/).  

##Bugs

HTTPS certificates are not authenticated (temporary solution implemented).  
Code examples currently out of sync.  

##Reference

[FreeFormatter.com](http://www.freeformatter.com/xpath-tester.html) 
XPath Tester / Evaluator.  
[ZVON.org](http://zvon.org/comp/m/xpath.html) XPath resources.  
